﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelSingalsInstaller : Installer<LevelSingalsInstaller>
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);

        Container.DeclareSignal<SignalWinLevel>();
        Container.DeclareSignal<SignalChangeLevelState>();
    }
}
