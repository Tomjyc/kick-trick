﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class MouseDragAxis : ITouschScreenEvent, ITickable
{
    public float MouseAxisX { get => mouseAxisX; }
    float mouseAxisX;
    public float MouseAxisY { get => mouseAxisY; }
    float mouseAxisY;
    public Vector3 MousePosition { get => mousePosition; }
    Vector3 mousePosition;

    bool isMousePresed;
    public bool IsMousePresed { get => isMousePresed; }
    public Action ActionMouseUp { get => ActionMouseUp1; set => ActionMouseUp1 = value; }
    public Action ActionMouseDown { get => ActionMouseDown1; set => ActionMouseDown1 = value; }

    Action ActionMouseDown1;
    Action ActionMouseUp1;

    public void Tick()
    {
        EditorAxis();
        //MobileAxis();
    }

    void MobileAxis()
    {
        if (Input.touchCount == 0) return;

        if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;

            mouseAxisX = Input.GetAxis("Mouse X");
            mouseAxisY = Input.GetAxis("Mouse Y");

            mousePosition = Input.mousePosition;
        }

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;

            isMousePresed = true;
            ActionMouseDown?.Invoke();
        }

        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;

            mouseAxisX = 0;
            mouseAxisY = 0;

            ActionMouseUp?.Invoke();
            isMousePresed = false;
        }
    }

    void EditorAxis()
    {
        if (Input.GetMouseButton(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) return ;

            mouseAxisX = Input.GetAxis("Mouse X");
            mouseAxisY = Input.GetAxis("Mouse Y");

            mousePosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) return ;

            isMousePresed = true;
            ActionMouseDown?.Invoke();
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) return ;

            mouseAxisX = 0;
            mouseAxisY = 0;

            ActionMouseUp?.Invoke();
            isMousePresed = false;
        }
    }
    
    bool IfOnUI() {
        if (!IfEditor() && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return true;
        if (IfEditor() && EventSystem.current.IsPointerOverGameObject()) return true;

        return false;
    }

    bool IfEditor()
    {
        if (Application.isEditor)
            return true;
        else
            return false;
    }
}
