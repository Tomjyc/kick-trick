﻿using UnityEngine;

public class MoneyManager
{
    GameInstaller.Settings settings;

    string keyMoney = "money";
    string keyKristals = "kristals";

    public MoneyManager(GameInstaller.Settings settings)
    {
        this.settings = settings;
    }

    public void SaveMoney(int amount)
    {
        int money = GetMoney();
        money += amount;
        PlayerPrefs.SetInt(keyMoney, money);
    }
    public void SaveKristals(int amount)
    {
        int kristals = GetKristals();
        kristals += amount;
        PlayerPrefs.SetInt(keyKristals, kristals);
    }

    public int GetMoney() => PlayerPrefs.GetInt(keyMoney);
    public int GetKristals() => PlayerPrefs.GetInt(keyKristals);

    public int GetRewardForLevel() => settings.rewardForLevel;
}
