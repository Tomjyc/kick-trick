﻿using UnityEngine;

public class TimerCountDown 
{
    float leftTime = 0;
    public float Time { get => leftTime; set => leftTime = value; }

    public System.Action ActionTimeIsOut;

    public void Update()
    {
        if (leftTime <= 0) return;

        leftTime -= 1 * UnityEngine.Time.deltaTime;

        if (leftTime <= 0) ActionTimeIsOut?.Invoke(); 
    }
}
