﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public interface ITouschScreenEvent
{
    float MouseAxisX { get; }
    float MouseAxisY { get; }
    Vector3 MousePosition { get; }
    bool IsMousePresed { get; }
    Action ActionMouseUp { get; set; }
    Action ActionMouseDown { get; set; }
}

public class TouchTest : MonoBehaviour, ITouschScreenEvent, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public float MouseAxisX => mouseAxisX;
    public float MouseAxisY => mouseAxisY;
    float mouseAxisX, mouseAxisY;

    public Vector3 MousePosition => mousePosition;
    Vector3 mousePosition;

    public bool IsMousePresed { get => isMousePresed; }
    bool isMousePresed;

    public Action ActionMouseUp { get => actionMouseUp; set => actionMouseUp = value; }
    public Action ActionMouseDown { get => actionMouseDown; set => actionMouseDown = value; }
    Action actionMouseDown;
    Action actionMouseUp;

    public void OnDrag(PointerEventData eventData)
    {
        mouseAxisX = eventData.delta.x;
        mouseAxisY = eventData.delta.y;

        mousePosition = eventData.position;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        mousePosition = eventData.position;

        isMousePresed = true;
        ActionMouseDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        mouseAxisX = 0;
        mouseAxisY = 0;

        isMousePresed = false;

        ActionMouseUp?.Invoke();
    }
}
