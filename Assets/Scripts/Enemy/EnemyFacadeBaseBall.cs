﻿using Zenject;

public class EnemyFacadeBaseBall : EnemyFacade
{
    public DirectAngleReflector directAngleReflector;
    public ReflectorCircle reflectorCircle;

    //[Inject]
    //public void Construct(EnemyStateManager2 stateManager, RagDollController dollController, LevelFacade levelFacade)
    //{
    //    Construct(stateManager, dollController, levelFacade);
    //}

    private void Start()
    {
        directAngleReflector.ActionTriggerEnder += ReflectBallAction;
    }

    void ReflectBallAction()
    {
        ChangeState(EnemyStates.SpecialAbility);
    }

    public override void FriendDieReaction()
    {
        base.FriendDieReaction();

        reflectorCircle.DeInitialize();
        directAngleReflector.IsCaneReflect = false;
    }
    public override void HittedByBall() { }
}