﻿using PathCreation;
using UnityEngine;

public class FrontRuner : PathRuner
{
    public FrontRuner(Transform transform, PathCreator pathCreator, float speed, EndOfPathInstruction endOfPathInstruction) : base(transform, pathCreator, speed, endOfPathInstruction) { }

    public override void Move()
    {
        base.Move();

        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
    }
}