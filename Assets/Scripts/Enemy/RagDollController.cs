﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RagDollController : IInitializable
{
    Animator animator;
    Collider collider;
    List<Collider> colliders;
    List<Rigidbody> riggirs;

    public bool IsHited { get => hited; }
    bool hited = false;

    public RagDollController(Collider collider, Animator animator, List<Collider> colliders, List<Rigidbody> riggirs)
    {
        this.animator = animator;
        this.collider = collider;
        this.colliders = colliders;
        this.riggirs = riggirs;
    }

    public void Initialize()
    {
        foreach (Collider collider in colliders)
            collider.enabled = false;

        foreach (Rigidbody rigidbody in riggirs)
            rigidbody.useGravity = false;
    }

    public void OnRagDoll()
    {
        hited = true;

        collider.enabled = false;
        animator.enabled = false;

        foreach (Collider collider in colliders)
            collider.enabled = true;

        foreach (Rigidbody rigidbody in riggirs)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.useGravity = true;
        }
    }

    public void OffRagDoll()
    {
        hited = false;

        animator.enabled = true;
        collider.enabled = true;
        Rebind();

        foreach (Collider collider in colliders)
            collider.enabled = false;

        foreach (Rigidbody rigidbody in riggirs)
            rigidbody.useGravity = false;
    }

    internal void Rebind()
    {
        animator.Rebind();
    }
}
