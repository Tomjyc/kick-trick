﻿using Zenject;
using UnityEngine;
using System;

public interface IEnemy
{
    void HittedByBall();
    void Activate();
    void DeActivate();
}

public class EnemyFacade : MonoBehaviour, IEnemy, IReseteble
{
    EnemyStateManager2 stateManager;
    RagDollController dollController;
    LevelFacade levelFacade;

    [Inject]
    public void Construct(EnemyStateManager2 stateManager, RagDollController dollController, LevelFacade levelFacade)
    {
        this.dollController = dollController;
        this.stateManager = stateManager;
        this.levelFacade = levelFacade;
    }

    private void Awake()
    {
        DeActivate();
    }

    public void ChangeState(Enum @enum) 
    {
        stateManager.ChangeState(@enum);
    }

    public void LookAtPlayer()
    {
        transform.LookAt(levelFacade.Player.transform);
    }

    public virtual void FriendDieReaction()
    {
        ChangeState(EnemyStates.FriendHit);
    }
    public virtual void HittedByBall()
    {
        dollController.OnRagDoll();
        levelFacade.GetCurrentPlayArea().EnemiesReactOnFriendHit();
    }
    public virtual void ReSet()
    {
        if (dollController.IsHited)
            dollController.OffRagDoll();

        ChangeState(EnemyStates.Reset);
    }

    public virtual void Activate()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
    public virtual void DeActivate()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void PlayerOnAreaAction()
    {
        ChangeState(EnemyStates.PlayerOnArea);
    }
}