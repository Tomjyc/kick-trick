﻿using PathCreation;
using UnityEngine;

public class SideWoker : PathRuner
{
    Animator animator;

    int hashMirroAnimation;
    bool switcher;

    public SideWoker(Animator animator, Transform transform, PathCreator pathCreator, float speed, EndOfPathInstruction endOfPathInstruction) : base(transform, pathCreator, speed, endOfPathInstruction)
    {
        this.animator = animator;

        hashMirroAnimation = Animator.StringToHash("MirroAnimation");
    }

    public override void Move()
    {
        base.Move();

        float t = distanceTravelled / pathCreator.path.length;

        if (Mathf.PingPong(t, 1f) > .99f && !switcher)
        {
            Debug.Log("da");
            switcher = true;
            animator.SetBool(hashMirroAnimation, switcher);
        }
        if (Mathf.PingPong(t, 1f) < .01f && switcher)
        {
            Debug.Log("net");
            switcher = false;
            animator.SetBool(hashMirroAnimation, switcher);
        }
    }
}
