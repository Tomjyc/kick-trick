﻿using UnityEngine;
using Zenject;

public interface IPathRuner
{
    void PathRun();
}

public class EnemyFacadeRuner : EnemyFacade, IPathRuner
{
    IRuner runner;

    [Inject]
    public void Construct(EnemyStateManager2 stateManager, RagDollController dollController, LevelFacade levelFacade, IRuner runner)
    {
        Construct(stateManager, dollController, levelFacade);
        this.runner = runner;
    }

    public override void Activate()
    {
        base.Activate();

        runner.GetClosestPoint();
    }

    public void PathRun() => runner.Move();

    public override void ReSet()
    {
        base.ReSet();

        runner.GetClosestPoint();
    }
}