﻿using PathCreation;
using Zenject;
using UnityEngine;

public class PathRuner : IRuner
{
    protected Transform transform;
    protected PathCreator pathCreator;
    protected EndOfPathInstruction endOfPathInstruction;
    float speed;
    protected float distanceTravelled;

    public PathRuner(Transform transform,PathCreator pathCreator, float speed, EndOfPathInstruction endOfPathInstruction)
    {
        this.transform = transform;
        this.pathCreator = pathCreator;
        this.speed = speed;
        this.endOfPathInstruction = endOfPathInstruction;
    }

    public void GetClosestPoint()
    {
        distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
    }

    public virtual void Move()
    {
        if (pathCreator == null) return;

        distanceTravelled += speed * Time.deltaTime;
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
    }
}