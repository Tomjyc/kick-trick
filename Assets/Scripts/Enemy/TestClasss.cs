﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TestClasss : IInitializable
{
    List<Collider> colliders;

    public TestClasss(List<Collider> colliders)
    {
        this.colliders = colliders;
    }

    public void Initialize()
    {
        Debug.Log(colliders.Count + " Enemy colliders");
    }
}
