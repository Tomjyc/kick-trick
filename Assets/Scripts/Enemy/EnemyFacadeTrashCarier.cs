﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFacadeTrashCarier : EnemyFacadeRuner
{
    CarriedCup cup;
    float pushForce;
    
    private void Start()
    {
        EnemyTrashCarierInstaller installer = GetComponent<EnemyTrashCarierInstaller>();
        cup = installer.GetCup();
        pushForce = installer.GetPushForce();
    }

    public override void HittedByBall()
    {
        base.HittedByBall();
        cup.Push(pushForce);
    }

    public override void ReSet()
    {
        base.ReSet();

        cup.ReSet();
    }
}
