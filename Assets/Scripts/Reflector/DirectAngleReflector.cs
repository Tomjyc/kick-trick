﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectAngleReflector : TestDirectionReflector
{
    [SerializeField] Transform rotator;

    protected override void CollisionEnter(Collision collision)
    {
        directionPointer.position = rotator.position + rotator.rotation * Vector3.forward;

        base.CollisionEnter(collision);
    }
}
