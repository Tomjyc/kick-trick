﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectorCircle : MonoBehaviour, IInitialize
{
    public Transform rotator;
    public Quaternion RotatorRotation { get => rotator.rotation; set => rotator.rotation = value; }

    public float turnSpped = .1f;
    public float
        arcDirectionAngle,
        arcAnglesRange,
        arcRadius;
    public float[] angleShootPoints;

    bool rotate;
    int angleIndex = 0;
    bool switchFlag = true;
    float currentTargetAngle;

    Transform defaultRotatorValue;
    List<float> targetDirections;
    IEnumerator couratineChangeAngle;

    private void Awake()
    {
        defaultRotatorValue = rotator;
    }

    private void Start()
    {
        targetDirections = new List<float>();
        FillPool();
        currentTargetAngle = targetDirections[0];
    }

    void FillPool()
    {
        float[] sortPoints = angleShootPoints;
        Array.Sort(sortPoints);
        targetDirections.Add(-arcAnglesRange / 2);
        for (int i = 0; i < sortPoints.Length; i++)
        {
            targetDirections.Add(sortPoints[i]);
        }
        targetDirections.Add(arcAnglesRange / 2);
    }

    private void Update()
    {
        if (!rotate) return;

        Quaternion goalRotation = GetGoalRotatation();
        goalRotation *= Quaternion.Euler(rotator.rotation.eulerAngles.x, 0, rotator.rotation.eulerAngles.z);
        rotator.rotation = Quaternion.RotateTowards(rotator.rotation, goalRotation, turnSpped * Time.deltaTime);

        AngleSwitcher(goalRotation);
    }

    void AngleSwitcher(Quaternion goalRotation)
    {
        float angle = Quaternion.Angle(goalRotation, rotator.rotation);
        if (angle <= 0 && couratineChangeAngle == null)
        {
            couratineChangeAngle = ChangeAngle();
            StartCoroutine(couratineChangeAngle);
        }
    }

    public void Initialize()
    {
        rotate = true;
        currentTargetAngle = 0;
        angleIndex = 0;
        rotator = defaultRotatorValue;
        StopAllCoroutines();
        couratineChangeAngle = null;
    }
    public void DeInitialize()
    {
        rotate = false;
    }

    IEnumerator ChangeAngle()
    {
        yield return new WaitForSeconds(1f);

        currentTargetAngle = targetDirections[angleIndex];

        if (switchFlag) angleIndex++; else angleIndex--;
        if (angleIndex <= 0)
        {
            switchFlag = !switchFlag;
            angleIndex = 0;
        }
        if (angleIndex >= targetDirections.Count - 1)
        {
            switchFlag = !switchFlag;
            angleIndex = targetDirections.Count - 1;
        }

        couratineChangeAngle = null;
        
    }
    Quaternion GetGoalRotatation()
    {
        Vector3 dir = GetDirection(currentTargetAngle);
        Vector3 direction = (dir - rotator.position).normalized;
        return Quaternion.LookRotation(direction);
    }
    void LimitShootPoints()
    {
        for (int i = 0; i < angleShootPoints.Length; i++)
        {
            float arcRange = arcAnglesRange / 2;
            angleShootPoints[i] = Mathf.Clamp(angleShootPoints[i], -arcRange, arcRange);
        }
    }

    Vector3 GetDirection(float angle) 
    {
        angle += arcDirectionAngle;
        Vector3 angleVector = GetVector(angle);
        return transform.position + angleVector * arcRadius;
    }
    Vector3 GetVector(float angle) => Quaternion.Euler(0, angle, 0) * transform.rotation * Vector3.forward;

    private void OnDrawGizmos()
    {
        LimitShootPoints();

        Gizmos.color = Color.red;
        Vector3 arcDirection = GetDirection(arcDirectionAngle);
        foreach (float angle in angleShootPoints)
            Gizmos.DrawWireSphere(GetDirection(angle), .1f);

        Gizmos.color = Color.green;
        GizmosExtensions.DrawWireArc(transform.position, GetVector(arcDirectionAngle), arcAnglesRange, arcRadius);
    }
}
