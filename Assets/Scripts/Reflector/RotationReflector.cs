﻿using UnityEngine;

public class RotationReflector : MonoBehaviour, IInitialize
{

    ReflectorCircle reflectorCircle;
    DirectAngleReflector directAngleReflector;

    RotationReflector relatedReflector;

    public void Initialize()
    {
        reflectorCircle.Initialize();
        directAngleReflector.IsCaneReflect = true;
    }

    public void DeInitialize()
    {
        reflectorCircle.DeInitialize();
    }

    private void Awake()
    {
        reflectorCircle = GetComponent<ReflectorCircle>();
        directAngleReflector = GetComponent<DirectAngleReflector>();
    }

    void Update()
    {
        if(relatedReflector != null)
            relatedReflector.SetRotatorRotation = reflectorCircle.RotatorRotation;
    }

    public void AddRelatedReflector(RotationReflector relatedReflector)
    {
        this.relatedReflector = relatedReflector;
    }
    public Quaternion SetRotatorRotation { set => reflectorCircle.RotatorRotation = value; }
}
