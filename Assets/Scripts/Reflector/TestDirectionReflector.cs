﻿using UnityEngine;
using Zenject;

public class TestDirectionReflector : AngleReflector
{
    [SerializeField] protected Transform directionPointer;

    protected override void CollisionEnter(Collision collision)
    {
        reflector.SetNewBallDirection(collision.collider, reflectPower, transform, directionPointer);
        reflector.SetForceToRelatedObject(collision, rbReletedObject, prevCollideBallVelocity);
    }
}