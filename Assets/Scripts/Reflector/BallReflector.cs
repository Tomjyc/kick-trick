﻿using System;
using UnityEngine;

public class BallReflector : MonoBehaviour
{
    [SerializeField]
    protected float reflectPower = 1f;

    public bool IsCaneReflect { set => isCaneReflect = value; }
    bool isCaneReflect = true;

    public Action ActionCollisionEnder { get => actionCollisionEnder; set => actionCollisionEnder = value; }
    public Action ActionTriggerEnder { get => actionTriggerEnder; set => actionTriggerEnder = value; }
    Action actionCollisionEnder;
    Action actionTriggerEnder;

    protected Vector3 prevCollideBallVelocity;
    protected Reflector reflector = new Reflector();

    private void OnCollisionEnter(Collision collision)
    {
        if (!isCaneReflect) return;

        Ball ball = collision.gameObject.GetComponent<Ball>();
        if (ball == null) return;

        actionCollisionEnder?.Invoke();

        CollisionEnter(collision);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!isCaneReflect) return;

        Ball ball = other.gameObject.GetComponent<Ball>();
        if (ball == null) return;

        prevCollideBallVelocity = reflector.GetVelocity(other);

        actionTriggerEnder?.Invoke();

        TriggerREnter(other);
    }

    protected virtual void CollisionEnter(Collision collision) { }
    protected virtual void TriggerREnter(Collider other) { }
}
