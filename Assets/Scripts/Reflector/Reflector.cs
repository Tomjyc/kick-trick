﻿using UnityEngine;

public class Reflector
{
    public Vector3 GetVelocity(Collider other)
    {
        Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
        return rb.velocity;
    }
    public void SetNewBallDirection(Collider collider, float reflectPower, Transform startpoint, Transform directionPointer)
    {
        Rigidbody rbBall = collider.GetComponent<Rigidbody>();

        //prevCollideBallVelocity = rbBall.velocity;

        Vector3 dir = (directionPointer.position - startpoint.position).normalized;

        rbBall.velocity = dir * reflectPower;
    }
    public void SetForceToRelatedObject(Collision collision, Rigidbody rbReletedObject, Vector3 velicityToRelatedObj)
    {
        BallFacade ballFacade = collision.gameObject.GetComponent<BallFacade>();
        if (ballFacade != null && rbReletedObject != null)
        {
            ContactPoint contact = collision.contacts[0];
            rbReletedObject.velocity = Vector3.zero;
            //rbReletedObject.velocity = velicityToRelatedObj;
            rbReletedObject.AddForce(velicityToRelatedObj, ForceMode.Impulse);
            // rbReletedObject.AddForceAtPosition(prevCollideBallVelocity, contact.point, ForceMode.Impulse);
            //rbReletedObject.AddForceAtPosition(Vector3.Reflect(prevCollideBallVelocity, contact.normal), contact.point, ForceMode.Impulse);
        }
    }
    public void MirrorReflection(Collision collision, float reflectPower)
    {
        Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
        Vector3 normal = collision.contacts[0].normal;

        rb.AddForce(normal.normalized * -reflectPower, ForceMode.Impulse);
    }
}