﻿using Zenject;
using UnityEngine;

public class EnemyReflector : AngleReflector
{
    EnemyInstaller.Settings enemySettring;
    IEnemy enemyFacade;

    [Inject]
    public void Construct(EnemyInstaller.Settings enemySettring, IEnemy enemyFacade)
    {
        this.enemySettring = enemySettring;
        this.enemyFacade = enemyFacade;
        reflectPower = enemySettring.reflectionBallForce;
    }

    protected override void CollisionEnter(Collision collision)
    {
        enemyFacade.HittedByBall();
        prevCollideBallVelocity *= enemySettring.reflectionHitForce;

        base.CollisionEnter(collision);
    }
}
