﻿using UnityEngine;
using Zenject;

public class AngleReflector : BallReflector
{
    [SerializeField] protected Rigidbody rbReletedObject;

    protected override void CollisionEnter(Collision collision)
    {
        reflector.MirrorReflection(collision, reflectPower);
        reflector.SetForceToRelatedObject(collision, rbReletedObject, prevCollideBallVelocity);
    }
}