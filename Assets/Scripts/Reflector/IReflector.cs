﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IReflector
{
    void Reflect(Ball ball);
}
