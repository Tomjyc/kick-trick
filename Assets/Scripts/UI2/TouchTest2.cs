﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchTest2 : MonoBehaviour
{

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            // Check if finger is over a UI element
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                Debug.Log("Touched the UI");
            }
        }
    }

    private bool WasAButton()
    {
        UnityEngine.EventSystems.EventSystem ct
            = UnityEngine.EventSystems.EventSystem.current;

        Button button = ct.currentSelectedGameObject.GetComponent<Button>();
        if (button == null)
            return true;

        return false;
    }
}
