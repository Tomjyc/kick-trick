﻿using Zenject;
using UnityEngine;

public class CustomLastMarkerFactory : IFactory<LastMarker>
{
    readonly DiContainer container;
    readonly BallLauncherInstaller.Settings settings;

    public CustomLastMarkerFactory(DiContainer container, BallLauncherInstaller.Settings settings)
    {
        this.container = container;
        this.settings = settings;
    }

    public LastMarker Create()
    {
        return container.InstantiatePrefabForComponent<LastMarker>(settings.lastMarker);
    }
}
