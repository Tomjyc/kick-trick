﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FixedTrajectory : TM
{
    string tagGroung = "Ground", tagBin = "Bin";
    int maxIterations = 200; // tmp

    Scene currentScene;
    Scene predictionScene;

    PhysicsScene currentPhysicsScene;
    PhysicsScene predictionPhysicsScene;

    Shooter shooter;
    TrajectoryLineRenderer lineRenderer;

    GameObject pref_dummyBall;
    BummyBall dummyBall;
    List<GameObject> dummyObstacles;
    Transform obstacles;

    Vector3 shootingPoint;

    int
        collideCounter = 0,
        trajectorySteps = 1,
        maxSteps = 3;

    public FixedTrajectory(TrajectoryLineRenderer lineRenderer, BallLauncherInstaller.Settings ballLauncherSettings, Shooter shooter)
    {
        this.shooter = shooter;
        this.lineRenderer = lineRenderer;
        pref_dummyBall = ballLauncherSettings.dummyBall;

        dummyObstacles = new List<GameObject>();

        currentScene = SceneManager.GetActiveScene();
        currentPhysicsScene = currentScene.GetPhysicsScene();

        CreateSceneParameters parameters = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
        predictionScene = SceneManager.CreateScene("Prediction", parameters);
        predictionPhysicsScene = predictionScene.GetPhysicsScene();
    }

    public void Initialize(Transform obstacles, Vector3 shootingPoint)
    {
        this.obstacles = obstacles;
        this.shootingPoint = shootingPoint;
        Physics.autoSimulation = false;

        lineRenderer.ShowMarker();
        CopyObstaclesToPredictionScene(obstacles);
        dummyBall = InstantiateDummyBall();
        dummyBall.DummyCollide += IncreaceCollide;
    }
    public void DeInitialize()
    {
        lineRenderer.positionCount = 0;
        Physics.autoSimulation = true;

        foreach (GameObject obstacle in dummyObstacles)
            GameObject.Destroy(obstacle);

        foreach(Transform obstacle in obstacles)
        {
            Rigidbody rb = obstacle.GetComponent<Rigidbody>();
            if (rb.gameObject.layer != LayerMask.NameToLayer("Obstacles")) continue;
            rb.isKinematic = false;
        }
        obstacles = null;

        dummyBall.DummyCollide -= IncreaceCollide;
        GameObject.Destroy(dummyBall.gameObject);
        dummyObstacles.Clear();
    }

    public void SimulationTime()
    {
        if (currentPhysicsScene.IsValid())
        {
            currentPhysicsScene.Simulate(Time.fixedDeltaTime);
        }
    }

    public void Predict(Quaternion shootVector, float shootPower)
    {
        if (!currentPhysicsScene.IsValid() && !predictionPhysicsScene.IsValid()) return;

        ResetDummyBall();

        lineRenderer.positionCount = maxIterations;
        lineRenderer.setPoints = ShootDummyBall(dummyBall, shootVector, shootPower);
    }
    void resetObstacles()
    {
        for (int i = 0; i < obstacles.childCount; i++)
        {
            dummyObstacles[i].transform.position = obstacles.GetChild(i).transform.position;
            dummyObstacles[i].transform.rotation = obstacles.GetChild(i).transform.rotation;
        }
    }
    void ResetDummyBall()
    {
        if (dummyBall == null) return;

        dummyBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        dummyBall.transform.position = shootingPoint;
    }

    Vector3[] ShootDummyBall(BummyBall dummyBall, Quaternion shootVector, float shootPower)
    {
        List<Vector3> trajectoryPoints = new List<Vector3>();

        shooter.Shoot(shootVector, dummyBall.gameObject, shootPower);
        for (int i = 0; i < maxIterations; i++)
        {
            //if (dummyBall.IsCollide) break;

            predictionPhysicsScene.Simulate(Time.fixedDeltaTime);
            trajectoryPoints.Add(dummyBall.transform.position);

            if (TrajectoryBreaker(dummyBall)) break;
        }

        return trajectoryPoints.ToArray();
    }
    bool TrajectoryBreaker(BummyBall dummyBall)
    {
        if (dummyBall.GetCollideTag.Equals(tagBin))
        {
            collideCounter = 0;
            return true;
        }
        else
            return false;
    }

    void CopyObstaclesToPredictionScene(Transform obstacles)
    {
        foreach (Transform t in obstacles)
        {
            Rigidbody rb = t.GetComponent<Rigidbody>();
            if (rb == null) continue;
            rb.isKinematic = true;

            GameObject fakeT = GameObject.Instantiate(t.gameObject);
            fakeT.transform.position = t.position;
            fakeT.transform.rotation = t.rotation;
            Renderer fakeR = fakeT.GetComponent<Renderer>();
            if (fakeR) fakeR.enabled = false;

            SceneManager.MoveGameObjectToScene(fakeT, predictionScene);
            dummyObstacles.Add(fakeT);
        }
    }
    BummyBall InstantiateDummyBall()
    {

        BummyBall dummyBall = GameObject.Instantiate(pref_dummyBall).GetComponent<BummyBall>();
        Renderer dummyR = dummyBall.GetComponent<Renderer>();
        if (dummyR) dummyR.enabled = false;
        SceneManager.MoveGameObjectToScene(dummyBall.gameObject, predictionScene);

        //dummyBall.IsCollide = false;

        return dummyBall;
    }
    void IncreaceCollide(string tag)
    {
        collideCounter++;
    }
    public void AddTrajectoryStep() { }
}
