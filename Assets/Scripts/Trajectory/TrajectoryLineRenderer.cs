﻿using UnityEngine;

public class TrajectoryLineRenderer
{
    LineRenderer lineRenderer;
    LastMarker lastMarker;

    public TrajectoryLineRenderer(LineRenderer lineRenderer, LastMarker.Factory lastMarkerFactory)
    {
        this.lineRenderer = lineRenderer;
        lastMarker = lastMarkerFactory.Create();
    }

    public void ShowMarker() => lastMarker.Show();
    public void HideMarker() => lastMarker.Hide();

    public int positionCount { set => lineRenderer.positionCount = value; }
    public Vector3[] setPoints { 
        set {
            positionCount = value.Length;
            lineRenderer.SetPositions(value);

            Vector3 posMarker = lineRenderer.GetPosition(lineRenderer.positionCount - 1);
            lastMarker.Position = new Vector3(posMarker.x, posMarker.y - .2f, posMarker.z);
        }
    }
}
