﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public interface TM
{
    void Initialize(Transform obstacles, Vector3 shootingPoint);
    void DeInitialize();
    void SimulationTime();
    void Predict(Quaternion shootVector, float shootPower);
    void AddTrajectoryStep();
}

public class TrajectoryManager : TM
{
    string tagGroung = "Ground", tagBin = "Bin";
    int maxIterations = 500; // tmp

    Scene currentScene;
    Scene predictionScene;

    PhysicsScene currentPhysicsScene;
    PhysicsScene predictionPhysicsScene;

    Shooter shooter;
    TrajectoryLineRenderer lineRenderer;

    GameObject lastMarker;
    
    List<GameObject> dummyObstacles;
    List<IInitialize> deInitializObjs;
    Transform obstacles;
    Vector3 shootingPoint;
    GameObject pref_dummyBall;

    int
        collideCounter = 0,
        trajectorySteps = 1,
        maxSteps = 3;
    Quaternion shootVector;
    float shootPower;

    public TrajectoryManager(TrajectoryLineRenderer lineRenderer, BallLauncherInstaller.Settings ballLauncherSettings, Shooter shooter)
    {
        this.shooter = shooter;
        this.lineRenderer = lineRenderer;
        pref_dummyBall = ballLauncherSettings.dummyBall;

        dummyObstacles = new List<GameObject>();
        deInitializObjs = new List<IInitialize>();

        currentScene = SceneManager.GetActiveScene();
        currentPhysicsScene = currentScene.GetPhysicsScene();

        CreateSceneParameters parameters = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
        predictionScene = SceneManager.CreateScene("Prediction", parameters);
        predictionPhysicsScene = predictionScene.GetPhysicsScene(); 
    }

    public void Initialize(Transform obstacles, Vector3 shootingPoint)
    {
        this.obstacles = obstacles;
        this.shootingPoint = shootingPoint;

        Physics.autoSimulation = false;
        trajectorySteps = 3;

        lineRenderer.positionCount = maxIterations;
        lineRenderer.ShowMarker();
        CopyObstaclesToPredictionScene(obstacles);
        
    }
    public void DeInitialize()
    {
        lineRenderer.positionCount = 0;
        lineRenderer.HideMarker();

        Physics.autoSimulation = true;

        foreach (IInitialize obstacle in deInitializObjs)
            obstacle.DeInitialize();
        foreach (GameObject obstacle in dummyObstacles)
            GameObject.Destroy(obstacle);

        obstacles = null;

        dummyObstacles.Clear();
    }
    public void AddTrajectoryStep()
    {
        if (trajectorySteps < maxSteps)
        {
            trajectorySteps++;
            Predict();
        }
    }
    public void SimulationTime()
    {
        if (currentPhysicsScene.IsValid())
        {
            currentPhysicsScene.Simulate(Time.fixedDeltaTime);
        }
    }

    void Predict() => Predict(shootVector, shootPower);
    public void Predict(Quaternion shootVector, float shootPower)
    {
        if (!currentPhysicsScene.IsValid() && !predictionPhysicsScene.IsValid()) return;

        //ResetDummyBall();
        //resetObstacles(); // allways appdate obstcles in fake scene need only when build level 

        lineRenderer.setPoints = ShootDummyBall(shootVector, shootPower);
        
        this.shootVector = shootVector;
        this.shootPower = shootPower;
    }

    void resetObstacles()
    {
        for (int i = 0; i < obstacles.childCount; i++)
        {
            dummyObstacles[i].transform.position = obstacles.GetChild(i).transform.position;
            dummyObstacles[i].transform.rotation = obstacles.GetChild(i).transform.rotation;
        }
    }
    void ResetDummyBall()
    {
        //if (dummyBall == null) return;

        //Rigidbody rb = dummyBall.GetComponent<Rigidbody>();
        //rb.velocity = Vector3.zero;
        //rb.angularVelocity = Vector3.zero;
        //dummyBall.transform.position = shootingPoint;
    }
    BummyBall InstantiateDummyBall()
    {
        BummyBall dummyBall = GameObject.Instantiate(pref_dummyBall).GetComponent<BummyBall>();
        Renderer dummyR = dummyBall.GetComponent<Renderer>();
        if (dummyR) dummyR.enabled = false;
        SceneManager.MoveGameObjectToScene(dummyBall.gameObject, predictionScene);

        //dummyBall.IsCollide = false;

        return dummyBall;
    }
    Vector3[] ShootDummyBall(Quaternion shootVector, float shootPower)
    {
        List<Vector3> trajectoryPoints = new List<Vector3>();

        BummyBall dummyBall = InstantiateDummyBall();
        dummyBall.transform.position = shootingPoint;
        dummyBall.DummyCollide += IncreaceCollide;

        shooter.Shoot(shootVector, dummyBall.gameObject, shootPower);
        for (int i = 0; i < maxIterations; i++)
        {
            //if (dummyBall.IsCollide) break;

            predictionPhysicsScene.Simulate(Time.fixedDeltaTime);
            trajectoryPoints.Add(dummyBall.transform.position);

            if (TrajectoryBreaker(dummyBall)) break;
        }

        dummyBall.DummyCollide -= IncreaceCollide;
        GameObject.Destroy(dummyBall.gameObject);

        return trajectoryPoints.ToArray();
    }

    void IncreaceCollide(string tag)
    {
        collideCounter++;
    }
    bool TrajectoryBreaker(BummyBall dummyBall)
    {
        if (dummyBall.GetCollideTag.Equals(tagBin) || collideCounter >= trajectorySteps)
        {
            collideCounter = 0;
            return true;
        }
        else
            return false;
    }

    void CopyObstaclesToPredictionScene(Transform obstacles)
    {
        foreach (Transform t in obstacles)
        {
            GameObject fakeT = GameObject.Instantiate(t.gameObject, t.position, t.rotation);

            if (t.TryGetComponent<RotationReflector>(out RotationReflector reflectorCircle))
            {
                fakeT.GetComponent<RotationReflector>().Initialize();
                fakeT.GetComponent<RotationReflector>().AddRelatedReflector(reflectorCircle);
                deInitializObjs.Add(fakeT.GetComponent<RotationReflector>());
            }

            Renderer fakeR = fakeT.GetComponent<Renderer>();
            if (fakeR) fakeR.enabled = false;
            Rigidbody rb = fakeT.GetComponent<Rigidbody>();
            if (rb != null) rb.isKinematic = true;

            SceneManager.MoveGameObjectToScene(fakeT, predictionScene);
            dummyObstacles.Add(fakeT);
        }
    }

}