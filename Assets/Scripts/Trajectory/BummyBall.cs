﻿using System;
using UnityEngine;

public class BummyBall : Ball
{
    public event Action<string> DummyCollide;

    public string GetCollideTag { get => collideTag; }
    string collideTag = "";

    private void OnCollisionEnter(Collision collision)
    {
        collideTag = collision.gameObject.tag;

        DummyCollide?.Invoke(collideTag);
    }

    private void OnCollisionExit(Collision collision)
    {
        collideTag = "";
    }
}
