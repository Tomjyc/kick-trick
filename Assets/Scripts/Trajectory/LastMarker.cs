﻿using Zenject;
using UnityEngine;

public class LastMarker : MonoBehaviour
{
    public Vector3 Position { set => transform.position = value; }

    private void Start()
    {
        Hide();
    }

    public void Show() => gameObject.SetActive(true);
    public void Hide() => gameObject.SetActive(false);

    public class Factory : PlaceholderFactory<LastMarker> { }
}