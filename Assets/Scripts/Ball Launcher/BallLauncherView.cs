﻿using UnityEngine;
using Zenject;

public class BallLauncherView : MonoBehaviour
{
    [HideInInspector]
    public Quaternion shootVector;
    [HideInInspector]
    public float shootPower;

    public LastMarker marker { get; private set; }

    public Vector3 Position { get => transform.position; set => transform.position = value; }
}
