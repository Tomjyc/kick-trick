﻿using UnityEngine;

public interface Aiming 
{
    Quaternion Aming(Quaternion rotator);
    float ShootPower();
}
