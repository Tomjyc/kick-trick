﻿using UnityEngine;
using Zenject;

public class Launcher : IFixedTickable, ITickable
{
    readonly TrajectoryManager trajectory;
    readonly Aiming aiming;

    public Launcher(TrajectoryManager trajectory, Aiming aiming)
    {
        this.trajectory = trajectory;
        this.aiming = aiming;


    }

    public void FixedTick()
    {
        //trajectory.Predict();
    }

    public void Tick()
    {
        //aiming.Aming();
    }
}
