﻿using UnityEngine;

public class AimingPointRotator : Aiming
{
    readonly ITouschScreenEvent mouseAxis;
    readonly GM gameManager;

    float
        rotationSpeed,
        angleLimit;

    public AimingPointRotator(GM gameManager, ITouschScreenEvent mouseAxis, BallLauncherInstaller.Settings settings)
    {
        this.gameManager = gameManager;
        this.mouseAxis = mouseAxis;

        rotationSpeed = settings.rotationSpeed;
        angleLimit = settings.anglelimit;
    }

    public Quaternion Aming(Quaternion rotator)
    {
        Quaternion prevStep = rotator;
        float rotateDegrees = mouseAxis.MouseAxisX * rotationSpeed * Mathf.Deg2Rad;
        rotator.eulerAngles += Vector3.up * rotateDegrees * Time.deltaTime;

        rotator = LimitRotation(rotator, prevStep);

        return rotator;
    }

    Quaternion LimitRotation(Quaternion pointRotation, Quaternion prevStep)
    {
        if (pointRotation.y > angleLimit * Mathf.Deg2Rad || pointRotation.y < -angleLimit * Mathf.Deg2Rad)
            return prevStep;
        else
            return pointRotation;
    }

    public float ShootPower() => gameManager.CurrentLevel.CurrentPlayArea.shootPower;
}
