﻿using UnityEngine;

public class Shooter 
{
    public void Shoot(Quaternion anglePoint, GameObject ball, float power)
    {
        Vector3 vector = anglePoint * Vector3.forward * power;
        ball.GetComponent<Rigidbody>().AddForce(vector, ForceMode.Impulse);
    }
}
