﻿using Zenject;
using UnityEngine;

public class BallLauncherFacade : MonoBehaviour
{
    public Vector3 Position { get => launcherView.Position; set => launcherView.Position = value; }
    
    public BallFacade Ball { get => ball; }
    BallFacade ball;

    TM trajectoryManager;
    GM gameManager;
    BLSM stateManager;
    GameInstaller.Settings gameSettings;
    BallLauncherView launcherView;
    BallFacade.Factory ballFactory;

    Shooter shooter;
    
    [Inject]
    public void Construct(BLSM stateManager, BallLauncherView launcherView, Shooter shooter, BallFacade.Factory ballFactory, GM gameManager, GameInstaller.Settings gameSettings, TM trajectoryManager)
    {
        this.gameSettings = gameSettings;
        this.stateManager = stateManager;
        this.launcherView = launcherView;
        this.shooter = shooter;
        this.ballFactory = ballFactory;
        this.gameManager = gameManager;
        this.trajectoryManager = trajectoryManager;
    }

    private void Start()
    {
        stateManager.ChangeState(BallLauncherStates.Wait);
    }

    public void AddTrajectoryStep() => trajectoryManager.AddTrajectoryStep();

    public void Shoot(int ballIndex)
    {
        ball = ballFactory.Create(gameSettings.balls[ballIndex]);
        ball.Position = Position;
        shooter.Shoot(launcherView.shootVector, ball.gameObject, launcherView.shootPower);

        gameManager.CurrentLevel.ChangeState = LevelStates.WaitWinOrLose;
    }

    public void SetAiming(bool isAiming)
    {
        if (isAiming)
        {
            stateManager.ChangeState(BallLauncherStates.Aiming);
        }
        else
        {
            stateManager.ChangeState(BallLauncherStates.Wait);
        }
    }
}
