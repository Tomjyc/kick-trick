﻿using UnityEngine;

public class ScreenWidthAimimgRotator : Aiming
{
    readonly ITouschScreenEvent mouseAxis;
    readonly GM gameManager;

    float angleLimit;
    float width;

    public ScreenWidthAimimgRotator(GM gameManager, ITouschScreenEvent mouseAxis, BallLauncherInstaller.Settings settings)
    {
        this.gameManager = gameManager;
        this.mouseAxis = mouseAxis;

        angleLimit = settings.anglelimit;

        width = Screen.width;
    }

    public Quaternion Aming(Quaternion rotator)
    {
        if (!mouseAxis.IsMousePresed) return rotator;

        float x = mouseAxis.MousePosition.x;
        x /= (width);
        x = Mathf.Clamp(x, 0, 1);
        x = Mathf.Lerp(-angleLimit, angleLimit, x);

        rotator = Quaternion.Euler(rotator.eulerAngles.x, x, rotator.eulerAngles.z);
        return rotator;
    }

    public float ShootPower() => gameManager.CurrentLevel.CurrentPlayArea.shootPower;
}
