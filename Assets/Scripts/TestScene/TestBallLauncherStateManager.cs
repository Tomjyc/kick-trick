﻿using System;
using Zenject;

public class TestBallLauncherStateManager : StateManager, BLSM
{
    StateWait wait;
    TestAmingState aiming;

    [Inject]
    public void Construct(StateWait wait, TestAmingState aiming)
    {
        this.wait = wait;
        this.aiming = aiming;
    }

    public override void ChangeState(Enum @enum)
    {
        switch (@enum)
        {
            case BallLauncherStates.Wait:
                ChangeState(wait);
                break;
            case BallLauncherStates.Aiming:
                ChangeState(aiming);
                break;
        }
    }
}
