using UnityEngine;
using Zenject;

public class TestBallLauncherInstaller : MonoInstaller
{
    [SerializeField] LineRenderer lineRenderer;

    public override void InstallBindings()
    {
        Container.Bind<Shooter>().AsCached();

        Container.Bind<Aiming>().To<TestPointRotator>().AsSingle();
        Container.Bind<TM>().To<TrajectoryManager>().AsSingle();
        Container.Bind<TrajectoryLineRenderer>().AsSingle().WithArguments(lineRenderer);

        Container.Bind<StateWait>().AsSingle();
        Container.Bind<TestAmingState>().AsSingle();

        Container.BindInterfacesAndSelfTo<TestBallLauncherStateManager>().AsSingle();
    }
}