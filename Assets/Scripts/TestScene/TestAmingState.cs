﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TestAmingState : IState
{
    GM gameManager;
    BallLauncherView launcherView;

    TM trajectory;
    Aiming aiming;

    Transform aimingVector;
    float shootPower;

    Quaternion currentRotation;

    [Inject]
    public void Construct(TM trajectory, Aiming aiming, GM gameManager, BallLauncherView launcherView)
    {
        this.trajectory = trajectory;
        this.aiming = aiming;
        this.gameManager = gameManager;
        this.launcherView = launcherView;
    }

    public void Enter()
    {
        PlayArea playArea = gameManager.CurrentLevel.CurrentPlayArea;
        aimingVector = playArea.aimingPoint;
        trajectory.Initialize(playArea.obstacles, playArea.aimingPoint.position);
    }

    public void Exit()
    {
        trajectory.DeInitialize();

        launcherView.shootPower = shootPower;
        launcherView.shootVector = aimingVector.rotation;
    }

    public void FixedUpdate() 
    {
        trajectory.SimulationTime();
    }

    public void Update()
    {
        aimingVector.rotation = aiming.Aming(aimingVector.rotation);
        shootPower = aiming.ShootPower();

        if (currentRotation != aimingVector.rotation)
        {
        }

        trajectory.Predict(aimingVector.rotation, shootPower);

        currentRotation = aimingVector.rotation;
    }
}
