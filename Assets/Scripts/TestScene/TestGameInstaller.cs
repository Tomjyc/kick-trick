using UnityEngine;
using Zenject;

public class TestGameInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        //Container.BindInterfacesAndSelfTo<MouseDragAxis>().AsSingle();
        Container.BindInterfacesAndSelfTo<TimerCountDown>().AsCached();

        Container.BindFactory<GameObject, BallFacade, BallFacade.Factory>().FromFactory<CustomBallFactory>();
        Container.BindFactory<int, LevelFacade, LevelFacade.Factory>().FromFactory<CustomLevelFactory>();
        Container.BindFactory<int, PlayerFacade, PlayerFacade.Factory>().FromFactory<CustomPlayerFactory>();
        Container.BindFactory<LastMarker, LastMarker.Factory>().FromFactory<CustomLastMarkerFactory>();

        LevelSingalsInstaller.Install(Container);
    }
}