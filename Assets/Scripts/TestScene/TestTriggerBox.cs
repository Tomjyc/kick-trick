﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTriggerBox : MonoBehaviour
{
    public bool isTrig = false;

    private void OnTriggerEnter(Collider other)
    {
        Ball ball = other.gameObject.GetComponent<Ball>();
        if (ball == null) return;

        isTrig = true;
    }

    private void OnTriggerExit(Collider other)
    {
        Ball ball = other.gameObject.GetComponent<Ball>();
        if (ball == null) return;

        isTrig = false;
    }
}
