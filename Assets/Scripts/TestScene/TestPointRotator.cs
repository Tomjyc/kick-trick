﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPointRotator : Aiming
{
    GM gameManager;

    float
        rotationSpeed,
        angleLimit;

    public TestPointRotator(GM gameManager)
    {
        this.gameManager = gameManager;

        rotationSpeed = 100f;
        angleLimit = 30f;
    }

    public Quaternion Aming(Quaternion rotator)
    {
        Quaternion prevStep = rotator;

        rotator = LimitRotation(rotator, prevStep);

        return rotator;
    }

    Quaternion LimitRotation(Quaternion pointRotation, Quaternion prevStep)
    {
        if (pointRotation.y > angleLimit * Mathf.Deg2Rad || pointRotation.y < -angleLimit * Mathf.Deg2Rad)
            return prevStep;
        else
            return pointRotation;
    }

    public float ShootPower()
    {
        return gameManager.CurrentLevel.CurrentPlayArea.shootPower;
    }
}
