﻿using UnityEngine;

public interface GM
{
    LevelFacade CurrentLevel { get; }
}

public class TestGameManager : MonoBehaviour, GM
{
    public LevelFacade CurrentLevel { get => currentLevel; }

    LevelFacade currentLevel;

    void Awake()
    {
        currentLevel = FindObjectOfType<LevelFacade>();
    }
}