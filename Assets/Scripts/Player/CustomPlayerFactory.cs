﻿using UnityEngine;
using Zenject;

public class CustomPlayerFactory : IFactory<int, PlayerFacade>
{
    DiContainer container;
    GameInstaller.Settings gameSettings;

    public CustomPlayerFactory(DiContainer container, GameInstaller.Settings gameSettings)
    {
        this.container = container;
        this.gameSettings = gameSettings;
    }

    public PlayerFacade Create(int playerIndex)
    {
        GameObject pref_player = gameSettings.players[playerIndex - 1];

        return container.InstantiatePrefabForComponent<PlayerFacade>(pref_player);
    }
}
