﻿using System;
using UnityEngine;
using Zenject;

public class PlayerFacade : MonoBehaviour
{
    public GameObject objToHide;
    public int ballIndex;

    PlayerView playerView;
    PlayerStateManager stateManager;

    [Inject]
    public void Construct(PlayerStateManager stateManager, PlayerView playerView)
    {   
        this.stateManager = stateManager;
        this.playerView = playerView;
    }

    public PlayerStates ChangeState { set => stateManager.ChangeState(value); }
    public bool SetActive { set => playerView.SetActive = value; }
    public Vector3 SetPosition { set => playerView.SetPosition = value; }

    public void Destroy() => Destroy(gameObject);
    public void HideBall() => objToHide.SetActive(false);
    public void ShowBall() => objToHide.SetActive(true);

    public void Reset()
    {
        ChangeState = PlayerStates.Restart;
        ShowBall();
    }
    internal void WinArea()
    {
        ChangeState = PlayerStates.Win;
    }

    public class Factory : PlaceholderFactory<int, PlayerFacade> { }
}
