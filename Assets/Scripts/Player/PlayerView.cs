﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public Animator animator;

    public Vector3 SetPosition { set => transform.position = value; }
    public bool SetActive { set => gameObject.SetActive(value); }
}
