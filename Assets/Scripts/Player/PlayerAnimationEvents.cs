﻿using Zenject;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour
{
    GM gameManager;
    PlayerFacade playerFacade;
    BallLauncherFacade ballLauncher;

    [Inject]
    public void Construct(PlayerFacade playerFacade, BallLauncherFacade ballLauncher, GM gameManager)
    {
        this.playerFacade = playerFacade;
        this.ballLauncher = ballLauncher;
        this.gameManager = gameManager;
    }

    public void ShootBullet()
    {
        ballLauncher.Shoot(playerFacade.ballIndex);
        playerFacade.HideBall();
    }

    public void ToNextArea()
    {
        gameManager.CurrentLevel.ChangeState = LevelStates.ToNextArea;
    }
}
