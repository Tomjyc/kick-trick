﻿using Zenject;
using UnityEngine;

public class StateAiming : IState
{
    GM gameManager;
    BallLauncherView launcherView;

    TM trajectory;
    Aiming aiming;

    Quaternion aimingVector;
    float shootPower;
    bool isActivePrediction;

    Quaternion currentRotation;

    [Inject]
    public void Construct(TM trajectory, Aiming aiming, GM gameManager, BallLauncherView launcherView)
    {
        this.trajectory = trajectory;
        this.aiming = aiming;
        this.gameManager = gameManager;
        this.launcherView = launcherView;
    }

    public void Enter()
    {
        aimingVector = new Quaternion();
        currentRotation = new Quaternion();

        PlayArea playArea = gameManager.CurrentLevel.CurrentPlayArea;
        aimingVector = playArea.aimingPoint.rotation; // set aming angle 

        isActivePrediction = playArea.isActivePrediction;

        trajectory.Initialize(playArea.obstacles, playArea.aimingPoint.position);
    }

    public void Exit()
    {
        trajectory.DeInitialize();

        launcherView.shootPower = shootPower;
        launcherView.shootVector = aimingVector;
    }

    public void Update() 
    {
        aimingVector = aiming.Aming(aimingVector);

        if (isActivePrediction || currentRotation != aimingVector)
        {
            shootPower = aiming.ShootPower();
            trajectory.Predict(aimingVector, shootPower);
        }
            
        currentRotation = aimingVector;
    }

    public void FixedUpdate() 
    {
        trajectory.SimulationTime();
    }
}
