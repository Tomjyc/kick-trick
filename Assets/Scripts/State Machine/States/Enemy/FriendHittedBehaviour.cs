﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendHittedBehaviour : StateMachineBehaviour
{
    EnemyFacade enemyFacade;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyFacade = animator.GetComponentInParent<EnemyFacade>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyFacade.LookAtPlayer();
    }
}
