﻿using Zenject;

public class StateEnemyHit : IState
{
    RagDollController dollController;

    [Inject]
    public void Construct(RagDollController dollController)
    {
        this.dollController = dollController;
    }

    public void Enter()
    {
        
    }

    public void Exit()
    {

    }

    public void FixedUpdate()
    {

    }

    public void Update()
    {

    }
}
