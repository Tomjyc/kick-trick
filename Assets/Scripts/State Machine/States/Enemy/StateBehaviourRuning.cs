﻿using UnityEngine;
using Zenject;

public class StateBehaviourRuning : StateMachineBehaviour
{
    IPathRuner enemyFacade;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyFacade = animator.GetComponentInParent<IPathRuner>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(enemyFacade != null) enemyFacade.PathRun();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyFacade = null;
    }
}
