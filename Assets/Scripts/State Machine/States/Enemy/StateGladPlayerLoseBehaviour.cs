﻿using UnityEngine;

public class StateGladPlayerLoseBehaviour : StateMachineBehaviour
{
    EnemyFacade enemyFacade;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyFacade = animator.GetComponentInParent<EnemyFacade>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyFacade.LookAtPlayer();
    }
}
