﻿using Zenject;
using UnityEngine;

public class StateEnemyPathRuning : IState
{
    RagDollController dollController;

    [Inject]
    public void Construct(RagDollController dollController)
    {
        this.dollController = dollController;
    }

    public void Enter()
    {

        dollController.OffRagDoll();
    }

    public void Exit()
    {

    }

    public void FixedUpdate() { }

    public void Update() { }
}
