﻿using UnityEngine;
using Zenject;

public class StateRestarLevel : IState
{
    LevelStateManager levelStateManager;
    LevelView levelView;
    LevelFacade levelFacade;

    [Inject]
    public void Construct(LevelView levelView, LevelStateManager levelStateManager, LevelFacade levelFacade)
    {
        this.levelView = levelView;
        this.levelStateManager = levelStateManager;
        this.levelFacade = levelFacade;
    }

    public void Enter() 
    {
        Debug.Log("Reset level");

        levelView.ResetPlayAreas();
        levelFacade.DeactivateEnemies();
        levelView.playAreaIndex = 0;

        levelStateManager.ChangeState(LevelStates.FirstArea);
    }

    public void Exit() { }

    public void Update() { }

    public void FixedUpdate() { }
}
