﻿using UnityEngine;
using Zenject;

public class StateWinLevel : IState
{
    LevelFacade levelFacade;

    public StateWinLevel(LevelFacade levelFacade)
    {
        this.levelFacade = levelFacade;
    }

    public void Enter()
    {
        Debug.Log("Win Level");
        //signalBus.Fire<SignalWinLevel>();

        PlayerFacade player = levelFacade.Player;
        player.ChangeState = PlayerStates.WinLevel;

        levelFacade.playerWinLevel();
    }

    public void Exit()
    {

    }

    public void Update()
    {

    }

    public void FixedUpdate() { }
}
