﻿using UnityEngine;

public class StateWinPlayArea : IState
{
    LevelFacade levelFacade;
    LevelView levelView;

    public StateWinPlayArea(LevelFacade levelFacade, LevelView levelView)
    {
        this.levelFacade = levelFacade;
        this.levelView = levelView;
    }

    public void Enter() 
    {
        Debug.Log("WinPlayArea");
        levelFacade.PlayerWinArea();
        levelView.playAreaIndex++;
    }

    public void Exit() { }

    public void Update() { }

    public void FixedUpdate() { }
}
