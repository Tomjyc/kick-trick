﻿using UnityEngine;
using Zenject;

public class StateWaitWinOrLose : IState
{
    LevelView levelView;
    TimerCountDown timerCountDown, timerBall;
    LevelStateManager stateManager;
    BallLauncherFacade ballLauncher;

    CupFacade cupFacade;
    BallFacade ballFacade;

    [Inject]
    public void Construct(LevelView levelView, TimerCountDown timerCountDown, LevelStateManager stateManager, BallLauncherFacade ballLauncher)
    {
        this.levelView = levelView;
        this.timerCountDown = timerCountDown;
        timerBall = timerCountDown;
        this.stateManager = stateManager;
        this.ballLauncher = ballLauncher;
    }

    public void Enter() 
    {
        ballFacade = ballLauncher.Ball;

        cupFacade = levelView.GetCurrentPlayArea().cup;
        cupFacade.HitTheCup += HitTheCup;

        timerBall.Time = 3f;
        timerBall.ActionTimeIsOut += TimeIsOut;
        timerCountDown.Time = 8f;
        timerCountDown.ActionTimeIsOut += TimeIsOut;
    }

    public void Exit() 
    {
        cupFacade.HitTheCup -= HitTheCup;
        timerCountDown.ActionTimeIsOut -= TimeIsOut;

        ballLauncher.Ball.Destroy();
    }

    public void Update() 
    {
        timerCountDown.Update();
        BallToLongOnGround();
    }

    public void FixedUpdate() { }

    void HitTheCup()
    {
        if (levelView.IsLastArea())
            stateManager.ChangeState(LevelStates.WinLevel);
        else
            stateManager.ChangeState(LevelStates.WinPlayArea);
    }

    void TimeIsOut()
    {
        stateManager.ChangeState(LevelStates.LosePlayArea);
    }

    bool fl = true;
    void BallToLongOnGround()
    {
        if (ballFacade == null) return;

        if (ballFacade.IsOnGround)
        {
            if (fl)
            {
                timerBall.Time = 3f;
                fl = false;
            }

            timerBall.Update();
        }
        else if (!ballFacade.IsOnGround && !fl)
        {
            fl = true;
        }
    }
}
