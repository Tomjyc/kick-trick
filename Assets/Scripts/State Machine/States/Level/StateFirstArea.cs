﻿using Zenject;
using UnityEngine;

public class StateFirstArea : StateToNextArea
{
    public StateFirstArea(Camera camera, GameInstaller.Settings settings, LevelStateManager levelStateManager, LevelFacade levelFacade) : base(camera, settings, levelStateManager, levelFacade) {}

    public override void Enter()
    {
        base.Enter();

        Debug.Log("StateFirstArea");

        camera.transform.position = cameraTargetPosition;
        
    }

    public override void Update() 
    {
        levelStateManager.ChangeState(LevelStates.PlayArea);
    }
}
