﻿using UnityEngine;
using Zenject;

public class StateRestartPlayArea : IState
{
    LevelStateManager levelStateManager;
    LevelFacade levelFacade;

    [Inject]
    public void Construct(LevelFacade levelFacade, LevelStateManager levelStateManager)
    {
        this.levelFacade = levelFacade;
        this.levelStateManager = levelStateManager;
    }

    public void Enter() 
    {
        Debug.Log("Reset Area");

        PlayArea playArea = levelFacade.CurrentPlayArea;
        playArea.ResetObstacles();

        PlayerFacade player = levelFacade.Player;
        player.ChangeState = PlayerStates.Restart;
        player.ShowBall();

        levelStateManager.ChangeState(LevelStates.PlayArea);
    }

    public void Exit() { }

    public void Update() { }

    public void FixedUpdate() { }
}
