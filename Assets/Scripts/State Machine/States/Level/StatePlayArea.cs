﻿using UnityEngine;

public class StatePlayArea : IState
{
    Camera camera;
    BallLauncherFacade ballLauncher;
    LevelView levelView;

    PlayArea playArea;

    public StatePlayArea(Camera camera, LevelView levelView, BallLauncherFacade ballLauncher)
    {
        this.camera = camera;
        this.ballLauncher = ballLauncher;
        this.levelView = levelView;
    }

    public void Enter()
    {
        Debug.Log("Play Area");
        playArea = levelView.GetCurrentPlayArea();
        camera.transform.position = playArea.cameraPoint.position;
        ballLauncher.Position = playArea.aimingPoint.position;
    }

    public void Exit() { }

    public void Update() { }

    public void FixedUpdate() { }

}