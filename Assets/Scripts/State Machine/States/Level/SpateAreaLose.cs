﻿using UnityEngine;
using Zenject;

public class SpateAreaLose : IState
{
    LevelFacade levelFacade;

    [Inject]
    public void Construct(LevelFacade levelFacade)
    {
        this.levelFacade = levelFacade;
    }

    public void Enter() 
    {
        Debug.Log("Lose");

        levelFacade.PlayerLose();
    }

    public void Exit()
    {

    }

    public void Update() { }

    public void FixedUpdate() { }
}