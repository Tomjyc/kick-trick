﻿using System;
using UnityEngine;

public class StateToNextArea : IState
{
    protected Camera camera;
    GameInstaller.Settings settings;
    LevelFacade levelFacade;
    protected LevelStateManager levelStateManager;

    protected Vector3 cameraTargetPosition;

    public StateToNextArea(Camera camera, GameInstaller.Settings settings, LevelStateManager levelStateManager, LevelFacade levelFacade)
    {
        this.camera = camera;
        this.settings = settings;
        this.levelFacade = levelFacade;
        this.levelStateManager = levelStateManager;
    }

    public virtual void Enter()
    {
        Debug.Log("To Next Area");

        levelFacade.SetPrevPlayer();
        levelFacade.CreatePlayer();
        levelFacade.ActivateEnemies();

        cameraTargetPosition = levelFacade.GetCameraPoint();
        camera.transform.rotation = levelFacade.GetCameraRotation();
    }

    public void Exit()
    {
        levelFacade.EnemiesSeePlayer();

        levelFacade.DestroyPrevPlayer();
        levelFacade.DeactivatePreviousEnemies();
    }

    public virtual void Update()
    {
        MoveCameraToPoint();
        ChangeStateByDistance();
    }

    public void FixedUpdate() { }

    protected void MoveCameraToPoint()
    {
        float step = settings.CameraMovemetSpeed * Time.deltaTime;
        camera.transform.position = Vector3.MoveTowards(camera.transform.position, cameraTargetPosition, step);
    }
    void ChangeStateByDistance()
    {
        if(Vector3.Distance(cameraTargetPosition, camera.transform.position) < .1f)
            levelStateManager.ChangeState(LevelStates.PlayArea);
    }
}
