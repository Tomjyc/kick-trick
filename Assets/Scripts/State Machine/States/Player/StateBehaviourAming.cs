﻿using Zenject;
using UnityEngine;

public class StateBehaviourAming : StateMachineBehaviour
{
    PlayerFacade playerFacade;
    BallLauncherFacade ballLauncher;
    ITouschScreenEvent axis;

    [Inject]
    public void Construct(ITouschScreenEvent axis, PlayerFacade playerFacade, BallLauncherFacade ballLauncher) 
    {
        this.axis = axis;
        this.playerFacade = playerFacade;
        this.ballLauncher = ballLauncher;
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ballLauncher.SetAiming(true);

        axis.ActionMouseUp += OnMouseUp;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ballLauncher.SetAiming(false);

        axis.ActionMouseUp -= OnMouseUp;
    }

    void OnMouseUp()
    {
        playerFacade.ChangeState = PlayerStates.Kick;
    }

    private void OnDestroy()
    {
        if(axis != null)
            axis.ActionMouseUp -= OnMouseUp;
    }
}
