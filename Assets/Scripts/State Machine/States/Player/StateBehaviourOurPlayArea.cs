﻿using Zenject;
using UnityEngine;

public class StateBehaviourOurPlayArea : StateMachineBehaviour
{
    GM gameManager;
    TimerCountDown timerCountDown;

    [Inject]
    public void Construct(GM gameManager, TimerCountDown timerCountDown)
    {
        this.gameManager = gameManager;
        this.timerCountDown = timerCountDown;
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timerCountDown.Time = 1;
        timerCountDown.ActionTimeIsOut += TimeIsOut;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timerCountDown.ActionTimeIsOut -= TimeIsOut;
        PlayerFacade player = animator.GetComponentInParent<PlayerFacade>();
        player.Destroy();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timerCountDown.Update();
    }

    void TimeIsOut()
    {
        gameManager.CurrentLevel.ChangeState = LevelStates.ToNextArea;
    }

    private void OnDestroy()
    {
        if(timerCountDown != null)
            timerCountDown.ActionTimeIsOut -= TimeIsOut;
    }
}
