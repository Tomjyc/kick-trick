﻿using System;
using UnityEngine;
using Zenject;

public abstract class AnimationStateManager
{
    public abstract void ChangeState(Enum @enum);
}

public enum PlayerStates
{
    Restart,
    Win,
    Lose,
    Kick,
    WinLevel, 
    Wait
}

public class PlayerStateManager : AnimationStateManager
{
    Animator animator;

    int hashWin,
        hashLose,
        hashRestart,
        hashKick,
        hashWinLevel;

    [Inject]
    public void Construct(Animator animator)
    {
        this.animator = animator;

        hashWin = Animator.StringToHash("Win");
        hashLose = Animator.StringToHash("Lose");
        hashRestart = Animator.StringToHash("Restart");
        hashKick = Animator.StringToHash("Kick");
        hashWinLevel = Animator.StringToHash("Win Level");
    }

    public override void ChangeState(Enum @enum)
    {
        switch (@enum)
        {
            case PlayerStates.Win:
                animator.SetTrigger(hashWin);
                break;
            case PlayerStates.Lose:
                animator.SetTrigger(hashLose);
                break;
            case PlayerStates.Restart:
                animator.SetTrigger(hashRestart);
                break;
            case PlayerStates.Kick:
                animator.SetTrigger(hashKick);
                break;
            case PlayerStates.WinLevel:
                animator.SetTrigger(hashWinLevel);
                break;
        }
    }
}
