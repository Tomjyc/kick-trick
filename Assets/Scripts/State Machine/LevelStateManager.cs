﻿using System;
using Zenject;

public enum LevelStates
{
    WinLevel,
    PlayArea,
    ToNextArea, 
    WaitWinOrLose, 
    RestartPlayArea,
    WinPlayArea,
    LosePlayArea,
    RestartLevel, 
    FirstArea
}

public class LevelStateManager : StateManager
{
    SignalBus signalBus;

    StateWinLevel winLevel;
    StatePlayArea playArea;
    StateToNextArea toNextArea;
    StateWaitWinOrLose winOrLose;
    StateRestartPlayArea restartPlayArea;
    StateRestarLevel restarLevel;
    StateWinPlayArea winPlayArea;
    SpateAreaLose losePlayArea;
    StateFirstArea firstArea;

    [Inject]
    public void Construct(SignalBus signalBus, StatePlayArea playArea, StateToNextArea toNextArea, StateWinLevel winLevel, StateWaitWinOrLose winOrLose, StateRestartPlayArea restartPlayArea, StateWinPlayArea winPlayArea, SpateAreaLose losePlayArea, StateRestarLevel restarLevel, StateFirstArea firstArea)
    {
        this.signalBus = signalBus;

        this.playArea = playArea;
        this.toNextArea = toNextArea;
        this.winLevel = winLevel;
        this.winOrLose = winOrLose;
        this.restartPlayArea = restartPlayArea;
        this.winPlayArea = winPlayArea;
        this.losePlayArea = losePlayArea;
        this.restarLevel = restarLevel;
        this.firstArea = firstArea;
    }

    public override void ChangeState(Enum @enum)
    {
        switch (@enum)
        {
            case LevelStates.PlayArea:
                ChangeState(playArea);
                break;
            case LevelStates.ToNextArea:
                ChangeState(toNextArea);
                break;
            case LevelStates.WinLevel:
                ChangeState(winLevel);
                break;
            case LevelStates.WaitWinOrLose:
                ChangeState(winOrLose);
                break;
            case LevelStates.RestartPlayArea:
                ChangeState(restartPlayArea);
                break;
            case LevelStates.WinPlayArea:
                ChangeState(winPlayArea);
                break;
            case LevelStates.LosePlayArea:
                ChangeState(losePlayArea);
                break;
            case LevelStates.RestartLevel:
                ChangeState(restarLevel);
                break;
            case LevelStates.FirstArea:
                ChangeState(firstArea);
                break;
        }

        signalBus.Fire<SignalChangeLevelState>();
    }
}
