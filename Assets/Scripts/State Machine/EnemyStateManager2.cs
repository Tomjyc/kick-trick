﻿using System;
using Zenject;
using UnityEngine;

public enum EnemyStates
{
    PlayerOnArea,
    PlayerWin,
    FriendHit,
    Reset,
    PlayerLose,
    SpecialAbility
}

public class EnemyStateManager2 : AnimationStateManager
{
    Animator animator;

    int hashPlayerOnArea,
        hashPlayerWin,
        hashReset,
        hashFriendHit,
        hashPlayerLose,
        hashSpecialAbility;

    [Inject]
    public void Construct(Animator animator)
    {
        this.animator = animator;

        hashPlayerOnArea = Animator.StringToHash("PlayerOnArea");
        hashPlayerWin = Animator.StringToHash("PlayerWin");
        hashReset = Animator.StringToHash("Reset");
        hashFriendHit = Animator.StringToHash("FriendHit");
        hashPlayerLose = Animator.StringToHash("PlayerLose");
        hashSpecialAbility = Animator.StringToHash("SpecialAbility");
    }

    public override void ChangeState(Enum @enum)
    {
        switch (@enum)
        {
            case EnemyStates.PlayerOnArea:
                animator.SetTrigger(hashPlayerOnArea);
                break;
            case EnemyStates.PlayerWin:
                animator.SetTrigger(hashPlayerWin);
                break;
            case EnemyStates.FriendHit:
                animator.SetTrigger(hashFriendHit);
                break;
            case EnemyStates.PlayerLose:
                animator.SetTrigger(hashPlayerLose);
                break;
            case EnemyStates.Reset:
                animator.SetBool(hashReset, true);
                break;
            case EnemyStates.SpecialAbility:
                animator.SetBool(hashSpecialAbility, true);
                break;
        }
    }
}