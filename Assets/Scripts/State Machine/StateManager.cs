﻿using UnityEngine;
using Zenject;
using System;

public abstract class StateManager : ITickable, IFixedTickable
{
    public IState currentState { get; private set; }

    protected void ChangeState(IState state)
    {
        if (currentState == state) return; // All ready instate

        if (currentState != null) currentState.Exit();

        currentState = state;
        currentState.Enter();
    }

    public void Tick()
    {
        currentState.Update();
    }

    public abstract void ChangeState(Enum @enum);

    public void FixedTick()
    {
        currentState.FixedUpdate();
    }
}
