﻿using System;
using Zenject;

public enum BallLauncherStates
{
    Wait,
    Aiming
}

public interface BLSM
{
    void ChangeState(Enum @enum);
}

public class BallLauncherStateManager : StateManager, BLSM
{
    StateWait wait;
    StateAiming aiming;

    [Inject]
    public void Construct(StateWait wait, StateAiming aiming)
    {
        this.wait = wait;
        this.aiming = aiming;
    }

    public override void ChangeState(Enum @enum)
    {
        switch (@enum)
        {
            case BallLauncherStates.Wait:
                ChangeState(wait);
                break;
            case BallLauncherStates.Aiming:
                ChangeState(aiming);
                break;
        }
    }
}
