﻿using Zenject;
using System;

public enum EnemyStates1
{
    RunPath,
    Hited,
    Reset
}

public class EnemyStateManager : StateManager
{
    StateEnemyPathRuning pathRuning;
    StateEnemyHit enemyHit;
    StateEnemyReset enemyReset;

    [Inject]
    public void Construct(StateEnemyPathRuning pathRuning,StateEnemyHit enemyHit,StateEnemyReset enemyReset)
    {
        this.pathRuning = pathRuning;
        this.enemyHit = enemyHit;
        this.enemyReset = enemyReset;
    }

    public override void ChangeState(Enum @enum)
    {
        switch (@enum)
        {
            case EnemyStates1.RunPath:
                ChangeState(pathRuning);
                break;
            case EnemyStates1.Hited:
                ChangeState(enemyHit);
                break;
            case EnemyStates1.Reset:
                ChangeState(enemyReset);
                break;
        }
    }
}
