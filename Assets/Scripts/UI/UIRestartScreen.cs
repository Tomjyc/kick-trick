﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIRestartScreen : UIView
{
    [SerializeField]
    RadialBar radialBar;
    [SerializeField]
    Button btn_restartLevel;

    GameManager gameManager;

    [Inject]
    public void Construct(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }

    private void OnEnable()
    {
        btn_restartLevel.onClick.AddListener(() => RestartLevel());
        radialBar.ActionTimeIsOut += RewardTimeOut;
    }
    private void OnDisable()
    {
        btn_restartLevel.onClick.RemoveAllListeners();
        radialBar.ActionTimeIsOut -= RewardTimeOut;
        btn_restartLevel.gameObject.SetActive(false);
    }

    void RestartLevel()
    {
        gameManager.CurrentLevel.RestartLevel();
    }
    public void RestartArea()
    {
        Debug.Log(name + " Restart Area");
        gameManager.CurrentLevel.RestartArea();
    }

    void RewardTimeOut()
    {
        btn_restartLevel.gameObject.SetActive(true);
    }
}