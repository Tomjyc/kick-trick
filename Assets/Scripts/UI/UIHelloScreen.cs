﻿using Zenject;
using UnityEngine.UI;

public class UIHelloScreen : UIView
{
    public Button butonLevel;

    UIViewManager viewManager;
    ITouschScreenEvent axis;
    
    [Inject]
    public void Construct(UIViewManager viewManager, ITouschScreenEvent axis)
    {
        this.viewManager = viewManager;
        this.axis = axis;
    }

    private void Start()
    {
        butonLevel.onClick.AddListener(() => viewManager.OpenLevelSelectionScreen());
    }

    private void OnEnable()
    {
        //axis.ActionMouseDown += Hide;
    }
    private void OnDisable()
    {
        //axis.ActionMouseDown -= Hide;
    }

}
