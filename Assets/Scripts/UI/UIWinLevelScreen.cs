﻿using Zenject;
using UnityEngine;
using UnityEngine.UI;

public class UIWinLevelScreen : UIView
{
    public Button btn_nextLevel;
    public Text txt_rewardAmount;

    LevelSpawner levelSpawner;
    MoneyManager moneyManager;

    [Inject]
    public void Construct(LevelSpawner levelSpawner, MoneyManager moneyManager)
    {
        this.levelSpawner = levelSpawner;
        this.moneyManager = moneyManager;
    }

    void Start()
    {
        btn_nextLevel.onClick.AddListener(ToNextLevel);
    }

    void ToNextLevel()
    {
        int levelReward = moneyManager.GetRewardForLevel();
        moneyManager.SaveMoney(levelReward);
        levelSpawner.NextLevel();
    }

    void OnEnable()
    {
        txt_rewardAmount.text = moneyManager.GetRewardForLevel().ToString();
    }
}
