﻿using UnityEngine;
using Zenject;

public class UIViewManager : MonoBehaviour
{
    public UIView startView;
    public UIView[] uIViews;

    SignalBus signalBus;
    GameManager gameManager;

    [Inject]
    public void Construct(SignalBus signalBus, GameManager gameManager)
    {
        this.signalBus = signalBus;
        this.gameManager = gameManager;
    }

    private void Start()
    {
        HideAllViews();
        signalBus.Subscribe<SignalChangeLevelState>(ChangeLevelStatesListener);

    }

    public void ResetIndicators()
    {
        UIPlayScreen uIPlayScreen = GetView<UIPlayScreen>() as UIPlayScreen;

        uIPlayScreen.ResetIndecators();
        uIPlayScreen.ResetLevelText();
    }

    public void HideAllViews()
    {
        foreach (UIView view in uIViews) view.Hide();
    }
    public void Show<T>() where T : UIView
    {
        UIView view = GetView<T>();
        if(view != null) Show(view);
    }
    public void Hide<T>() where T : UIView
    {
        UIView view = GetView<T>();
        if (view != null) Hide(view);
    }
    public void Show(UIView view)
    {
        view.Show();
    }
    public void Hide(UIView view)
    {
        view.Hide();
    }
    UIView GetView<T>() where T : UIView
    {
        for (int i = 0; i < uIViews.Length; i++)
        {
            if (uIViews[i] is T)
            {
                return uIViews[i];
            }
        }

        return null;
    }

    public void ChangeLevelStatesListener()
    {
        IState state = gameManager.CurrentLevel.currentState;

        switch (state)
        {
            case StatePlayArea playArea:
                Hide<UIRestartScreen>();
                Show<UIPlayScreen>();
                ResetIndicators();
                break;
            case StateFirstArea firstArea:
                HideAllViews();
                Show<UIPlayScreen>();
                Show<UIHelloScreen>();
                break;
            case StateWinLevel winLevel:
                Show<UIWinLevelScreen>();
                Hide<UIPlayScreen>();
                break;
            case SpateAreaLose areaLose:
                Hide<UIPlayScreen>();
                Show<UIRestartScreen>();
                break;
        }
    }

    public void OpenLevelSelectionScreen()
    {
        Show<UILevelSelection>();

        Hide<UIPlayScreen>();
        Hide<UIHelloScreen>();
    }
    public void CloseLevelSelectionScreen()
    {
        Show<UIPlayScreen>();
        Show<UIHelloScreen>();

        Hide<UILevelSelection>();
    }
}