﻿using UnityEngine;
using DG.Tweening;
using Zenject;

public class AreaIndicator : MonoBehaviour
{
    public RectTransform imgIndicator;
    public Vector2 OnSize, OffSize;

    public void On()
    {
        imgIndicator.DOSizeDelta(OnSize, .5f, true);
    }

    public void Off()
    {
        imgIndicator.DOSizeDelta(OffSize, .5f, true);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public class Factory : PlaceholderFactory<AreaIndicator> { }
}
