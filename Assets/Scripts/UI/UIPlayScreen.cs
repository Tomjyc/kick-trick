﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIPlayScreen : UIView
{
    [SerializeField] Transform indicatorsParent;
    [SerializeField] Text textCurrentLevel, textNextLevel, txt_money;
    [SerializeField] Button btn_addTraejctory;

    MoneyManager moneyManager;
    GameManager gameManager;
    AreaIndicator.Factory areaIndicatorFactory;
    BallLauncherFacade ballLauncher;

    AreaIndicator[] indicators;

    [Inject]
    public void Construct(GameManager gameManager, AreaIndicator.Factory areaIndicatorFactory, MoneyManager moneyManager, BallLauncherFacade ballLauncher)
    {
        this.gameManager = gameManager;
        this.areaIndicatorFactory = areaIndicatorFactory;
        this.moneyManager = moneyManager;
        this.ballLauncher = ballLauncher;
    }

    void Start()
    {
        btn_addTraejctory.onClick.AddListener(() => AddTrajectory());
    }

    private void OnEnable()
    {
        txt_money.text = moneyManager.GetMoney().ToString();
        CreateIndicators();
        StartCoroutine(RefrashContentSizeFilter());
    }
    private void OnDisable()
    {
        ClearIndicators();
    }

    void AddTrajectory()
    {
        ballLauncher.AddTrajectoryStep();
    }

    IEnumerator RefrashContentSizeFilter()
    {
        yield return new WaitForEndOfFrame();
        indicatorsParent.GetComponent<ContentSizeFitter>().enabled = false;
        indicatorsParent.GetComponent<ContentSizeFitter>().enabled = true;
    }

    void CreateIndicators()
    {
        int areasLength = gameManager.CurrentLevel.AreasLength+1;
        indicators = new AreaIndicator[areasLength];

        for (int i = 0; i < areasLength; i++)
        {
            AreaIndicator indicator = areaIndicatorFactory.Create();
            indicator.transform.SetParent(indicatorsParent, false);
            indicators[i] = indicator;
        }
    }
    void ClearIndicators()
    {
        foreach (AreaIndicator item in indicators)
        {
            item.Destroy();
        }
    }

    public void ResetIndecators()
    {
        int currentLevelIndex = gameManager.CurrentLevel.currentAreaIndex;

        for(int i = 0; i < indicators.Length; i++)
        {
            if (i <= currentLevelIndex)
                indicators[i].On();
            else
                indicators[i].Off();
        }
    }
    public void ResetLevelText()
    {
        int currentLevelIndex = gameManager.CurrentLevelIndex;
        textCurrentLevel.text = (currentLevelIndex + 1).ToString();
        textNextLevel.text = (currentLevelIndex + 2).ToString();
    }
}