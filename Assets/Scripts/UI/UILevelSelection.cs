﻿using Zenject;
using UnityEngine;
using UnityEngine.UI;

public class UILevelSelection : UIView
{
    public Transform contentHolder;
    public Button buttonCloseView;

    GameInstaller.Settings gameSettings;
    GameManager gameManager;
    LevelSpawner levelSpawner;
    UIViewManager viewManager;

    GameObject[] levelButons;

    [Inject]
    public void Construct(GameInstaller.Settings gameSettings, GameManager gameManager, LevelSpawner levelSpawner, UIViewManager viewManager)
    {
        this.gameSettings = gameSettings;
        this.gameManager = gameManager;
        this.levelSpawner = levelSpawner;
        this.viewManager = viewManager;
    }

    private void Start()
    {
        buttonCloseView.onClick.AddListener(() => viewManager.CloseLevelSelectionScreen());
    }

    private void OnEnable()
    {
        SpawnLevelButtons();
    }

    private void OnDisable()
    {
        DespawnButtons();
    }

    void SpawnLevelButtons()
    {
        int levelsCount = gameSettings.levels.Length;
        levelButons = new GameObject[levelsCount];

        for (int i = 0; i < levelsCount; i++)
        {
            Button button = Instantiate(gameSettings.PrefLevelButton, contentHolder).GetComponent<Button>();
            int tmp = i;
            button.onClick.AddListener(() => LoadSelectedLevel(tmp));
            button.transform.GetChild(0).GetComponent<Text>().text = i.ToString();

            levelButons[i] = button.gameObject;
        }
    }
    void DespawnButtons()
    {
        foreach (GameObject button in levelButons)
        {
            Destroy(button);
        }
    }

    void LoadSelectedLevel(int levelIndex)
    {
        Hide();
        levelSpawner.SpawnLevel(levelIndex);
    }
}
