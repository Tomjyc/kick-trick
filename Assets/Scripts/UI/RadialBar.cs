﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DG.Tweening;

public class RadialBar : MonoBehaviour
{
    public System.Action ActionTimeIsOut { get => countDown.ActionTimeIsOut; set => countDown.ActionTimeIsOut = value; }
    
    [SerializeField] Image img_fill;
    [SerializeField] Text text_Count;
    [SerializeField] Image img_foreground;

    TimerCountDown countDown;
    int time;

    RectTransform rectTransform;

    Vector2 startRectSize;
    [SerializeField] Vector2 targetRectSize;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        startRectSize = rectTransform.sizeDelta;
    }

    [Inject]
    public void Construct(TimerCountDown countDown, GameInstaller.Settings settings)
    {
        this.countDown = countDown;
        time = settings.restartAdsTime;
    }

    private void OnEnable()
    {
        countDown.Time = time;
        ActionTimeIsOut += TimeIsOut;
    }

    private void OnDisable()
    {
        ActionTimeIsOut -= TimeIsOut;

        rectTransform.sizeDelta = startRectSize;
        Color tmpColor = img_foreground.color;
        tmpColor.a = 1f;
        img_foreground.color = tmpColor;

        text_Count.gameObject.SetActive(true);
    }

    void Update()
    {
        countDown.Update();

        float currentTime = countDown.Time;
        float interpolateTime = currentTime / time;

        img_fill.fillAmount = interpolateTime;
        text_Count.text = Mathf.Ceil(countDown.Time).ToString("0");
    }

    void TimeIsOut()
    {
        rectTransform.DOSizeDelta(new Vector2(600f, 600f), 1f);
        img_foreground.DOFade(0, 1f);
        text_Count.gameObject.SetActive(false);
    }
}
