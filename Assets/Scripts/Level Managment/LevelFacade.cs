﻿using System;
using UnityEngine;
using Zenject;

public class LevelFacade : MonoBehaviour
{
    public PlayArea CurrentPlayArea { get => levelView.GetCurrentPlayArea(); }
    public int AreasLength { get => levelView.AreasLength(); }
    public int currentAreaIndex { get => levelView.playAreaIndex; }
    public PlayerFacade Player { get => player; }
    public IState currentState { get => stateManager.currentState; }

    PlayerFacade.Factory playerFactory;
    LevelView levelView;
    LevelStateManager stateManager;
    BallLauncherFacade ballLauncher;

    PlayerFacade player, prevPlayer;

    [Inject]
    public void Constrct(LevelStateManager stateManager, LevelView levelView, BallLauncherFacade ballLauncher, PlayerFacade.Factory playerFactory)
    {
        this.stateManager = stateManager;
        this.levelView = levelView;
        this.ballLauncher = ballLauncher;
        this.playerFactory = playerFactory;
    }

    private void Start()
    {
        ChangeState = LevelStates.FirstArea;
    }

    public LevelStates ChangeState { set => stateManager.ChangeState(value); }
    public Vector3 GetCameraPoint() => levelView.GetCurrentPlayArea().cameraPoint.position;
    public Quaternion GetCameraRotation() => levelView.GetCurrentPlayArea().cameraPoint.rotation;

    public void ActivateEnemies()
    {
        GetCurrentPlayArea().ActivateEnemies();
    }
    public void DeactivateEnemies()
    {
        PlayArea area = levelView.GetCurrentPlayArea();
        area.DeActivateEnemies();
    }
    public void DeactivatePreviousEnemies()
    {
        int areaindex = levelView.playAreaIndex;
        if (areaindex > 0)
        {
            PlayArea area = levelView.GetPlayArea(areaindex - 1);
            area.DeActivateEnemies();
        }
    }
    public void EnemiesSeePlayer()
    {
        GetCurrentPlayArea().EnemiesSeePlayer();
    }

    public void PlayerWinArea()
    {
        GetCurrentPlayArea().EnemiesUpsadPlayerWin();

        player.WinArea();
    }
    public void playerWinLevel()
    {
        GetCurrentPlayArea().EnemiesUpsadPlayerWin();
    }
    public void PlayerLose()
    {
        player.ChangeState = PlayerStates.Lose;

        GetCurrentPlayArea().EnemiesGladPlayerLose();
    }

    public void SetPrevPlayer()
    {
        if (player != null)
            prevPlayer = player;
    }
    public void CreatePlayer()
    {
        player = playerFactory.Create(1);
        player.SetPosition = levelView.GetCurrentPlayArea().playerSpawnPoint.position;
    }
    public void DestroyPrevPlayer()
    {
        if (prevPlayer != null)
            prevPlayer.Destroy();
    }

    public PlayArea GetCurrentPlayArea()
    {
        return levelView.GetCurrentPlayArea();
    }

    public void RestartArea() 
    {
        player.Reset();

        GetCurrentPlayArea().ResetObstacles();
        stateManager.ChangeState(LevelStates.PlayArea);
    }
    //public void RestartArea() => stateManager.ChangeState(LevelStates.RestartPlayArea);
    public void RestartLevel() => stateManager.ChangeState(LevelStates.RestartLevel);
    public void DestroyLevel() 
    {
        ballLauncher.SetAiming(false);
        if (player != null) player.Destroy();
        if (prevPlayer != null) prevPlayer.Destroy();
        if (ballLauncher.Ball != null) ballLauncher.Ball.Destroy();

        Destroy(gameObject); 
    }

    public class Factory : PlaceholderFactory<int, LevelFacade> { }
}
