﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayArea : MonoBehaviour
{
    public bool isActivePrediction = false;
    public float shootPower = 2f;
    public Transform obstacles;
    public Transform cameraPoint;
    public Transform playerSpawnPoint;
    public Transform aimingPoint;
    public CupFacade cup;
    public Transform enemiesHolder;

    List<IReseteble> resetebles;
    List<IEnemy> enemies;

    bool isEnemyHit = false;

    private void Awake()
    {
        SetResetable();
        SetEnemies();
    }

    void SetEnemies()
    {
        enemies = new List<IEnemy>(enemiesHolder.GetComponentsInChildren<IEnemy>());      
    }
    void SetResetable()
    {
        resetebles = new List<IReseteble>();
        resetebles.AddRange(obstacles.GetComponentsInChildren<IReseteble>());
        resetebles.AddRange(enemiesHolder.GetComponentsInChildren<IReseteble>());
    }

    public void ResetObstacles()
    {
        foreach (IReseteble reseteble in resetebles)
            reseteble.ReSet();
    }
    
    public void EnemiesReactOnFriendHit()
    {
        foreach (EnemyFacade enemy in enemies)
            enemy.FriendDieReaction();

        isEnemyHit = true;
    }
    public void EnemiesGladPlayerLose()
    {
        if (!isEnemyHit)
            foreach (EnemyFacade enemy in enemies)
                enemy.ChangeState(EnemyStates.PlayerLose);
        else
            isEnemyHit = false;
    }
    public void EnemiesUpsadPlayerWin()
    {
        foreach (EnemyFacade enemy in enemies)
            enemy.ChangeState(EnemyStates.PlayerWin);
    }
    
    public void ActivateEnemies()
    {
        Debug.LogWarning("play area activate enemies " + enemies.Count);
        foreach (IEnemy enemy in enemies)
            enemy.Activate();
    }
    public void DeActivateEnemies()
    {
        foreach (IEnemy enemy in enemies)
            enemy.DeActivate();
    }

    public void EnemiesSeePlayer()
    {
        foreach (EnemyFacade enemy in enemies)
            enemy.PlayerOnAreaAction();
    }
}
