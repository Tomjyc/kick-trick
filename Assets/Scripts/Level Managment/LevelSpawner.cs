﻿using UnityEngine;

public class LevelSpawner
{
    readonly LevelFacade.Factory levelFactory;

    public LevelFacade currentLevel { private set; get; }
    public int currentLevelIndex { private set; get; }

    public LevelSpawner(LevelFacade.Factory levelFactory)
    {
        this.levelFactory = levelFactory;
    }

    public LevelFacade SpawnLevel(int levelIndex)
    {
        if (currentLevel != null) DestroyLevel();

        currentLevelIndex = levelIndex;
        currentLevel = levelFactory.Create(levelIndex);
        return currentLevel;
    }

    public void DestroyLevel()
    {
        currentLevel.DestroyLevel();
    }

    public void NextLevel()
    {
        SpawnLevel(currentLevelIndex + 1);
    }
}