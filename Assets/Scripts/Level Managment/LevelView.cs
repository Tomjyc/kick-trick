﻿using UnityEngine;
using Zenject;

public class LevelView : MonoBehaviour
{
    [SerializeField]
    PlayArea[] playAreas;

    [HideInInspector]
    public int playAreaIndex = 0;

    public void DestroyLevel() => Destroy(gameObject);

    public void ResetPlayAreas()
    {
        foreach (PlayArea area in playAreas)
        {
            area.ResetObstacles();
        }
    }

    public PlayArea GetPlayArea(int index) => playAreas[index];
    public PlayArea GetCurrentPlayArea() => GetPlayArea(playAreaIndex); 
    public int AreasLength() => playAreas.Length - 1;
    public bool IsLastArea() => playAreaIndex >= AreasLength();
}
