﻿using UnityEngine;
using Zenject;

public class CustomLevelFactory : IFactory<int, LevelFacade>
{
    DiContainer container;
    GameInstaller.Settings gameSettings;

    public CustomLevelFactory(DiContainer container, GameInstaller.Settings gameSettings)
    {
        this.container = container;
        this.gameSettings = gameSettings;
    }

    public LevelFacade Create(int levelIndex)
    {
        GameObject pref_level = gameSettings.levels[levelIndex];

        return container.InstantiatePrefabForComponent<LevelFacade>(pref_level);
    }
}
