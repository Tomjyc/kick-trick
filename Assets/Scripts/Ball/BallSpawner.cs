﻿using UnityEngine;

public class BallSpawner 
{
    readonly BallFacade.Factory ballFactory;

    public BallSpawner(BallFacade.Factory ballFactory)
    {
        this.ballFactory = ballFactory;
    }

    public BallFacade SpawnBall(GameObject pref_ball)
    {
        return ballFactory.Create(pref_ball);
    }
}
