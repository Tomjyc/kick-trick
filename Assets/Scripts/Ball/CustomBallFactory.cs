﻿using UnityEngine;
using Zenject;

public class CustomBallFactory : IFactory<GameObject, BallFacade>
{
    readonly DiContainer container;

    public CustomBallFactory(DiContainer container)
    {
        this.container = container;
    }

    public BallFacade Create(GameObject pref_ball)
    {
        return container.InstantiatePrefabForComponent<BallFacade>(pref_ball);
    }
}
