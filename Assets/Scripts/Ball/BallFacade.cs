﻿using Zenject;
using UnityEngine;

public class BallFacade : Ball
{
    public Vector3 Position { set => transform.position = value; }

    public class Factory : PlaceholderFactory<GameObject, BallFacade> { }
}