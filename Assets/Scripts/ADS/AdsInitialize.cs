﻿using UnityEngine;
using UnityEngine.Advertisements;
using Zenject;

public class AdsInitialize : IInitializable, IUnityAdsInitializationListener
{
    string androidGameId;
    string iOsGameId;
    bool testMode;
    bool enablePerPlacementMode;

    private string gameId;

    public AdsInitialize(GameInstaller.Settings settings)
    {
        androidGameId = settings._androidGameId;
        iOsGameId = settings._androidGameId;
        testMode = settings._testMode;
        enablePerPlacementMode = settings._enablePerPlacementMode;
    }

    public void Initialize()
    {
        InitializeAds();
    }

    public void InitializeAds()
    {
        gameId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? iOsGameId
            : androidGameId;
        Advertisement.Initialize(gameId, testMode, enablePerPlacementMode, this);
    }

    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }
}
