﻿public class ButtonRestartReward : AdsRewarded
{
    UIRestartScreen uIRestartScreen;

    protected override void Awake()
    {
        base.Awake();

        uIRestartScreen = GetComponentInParent<UIRestartScreen>();
    }

    public override void ShowAd()
    {
        uIRestartScreen.RestartArea();
    }

    protected override void RevordForAds()
    {
        uIRestartScreen.RestartArea();
    }
}
