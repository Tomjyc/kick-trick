﻿using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour, GM
{
    public LevelFacade CurrentLevel { get => levelSpawner.currentLevel; }
    public int CurrentLevelIndex { get => levelSpawner.currentLevelIndex; }

    int currentLevelIndex = 0;

    LevelSpawner levelSpawner;

    [Inject]
    public void Costruct(LevelSpawner levelSpawner)
    {
        this.levelSpawner = levelSpawner;
    }

    void Awake()
    {
        levelSpawner.SpawnLevel(currentLevelIndex);
    }
}
