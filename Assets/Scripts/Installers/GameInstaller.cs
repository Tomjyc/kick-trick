using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [Inject]
    Settings settings = null;

    public override void InstallBindings()
    {
        //Container.BindInterfacesAndSelfTo<MouseDragAxis>().AsSingle();
        Container.Bind<TimerCountDown>().AsTransient();

        Container.Bind<LevelSpawner>().AsSingle();
        Container.Bind<BallSpawner>().AsSingle();
        Container.Bind<MoneyManager>().AsSingle();

        Container.BindInterfacesAndSelfTo<AdsInitialize>().AsSingle();

        Container.BindFactory<GameObject, BallFacade, BallFacade.Factory>().FromFactory<CustomBallFactory>();
        Container.BindFactory<int, LevelFacade, LevelFacade.Factory>().FromFactory<CustomLevelFactory>();
        Container.BindFactory<int, PlayerFacade, PlayerFacade.Factory>().FromFactory<CustomPlayerFactory>();
        Container.BindFactory<LastMarker, LastMarker.Factory>().FromFactory<CustomLastMarkerFactory>();
        Container.BindFactory<AreaIndicator, AreaIndicator.Factory>().FromComponentInNewPrefab(settings.pref_areaIndicatort);

        LevelSingalsInstaller.Install(Container);
    }

    [System.Serializable]
    public class Settings
    {
        public int restartAdsTime = 3;
        public int rewardForLevel = 50;
        public float CameraMovemetSpeed = 5f;
        public GameObject[] levels;
        public GameObject[] players;
        public GameObject[] balls;
        public GameObject PrefLevelButton;
        public GameObject pref_areaIndicatort;

        [Header("ADS")]
        public string _androidAdUnitId = "Rewarded_Android";
        public string _iOsAdUnitId = "Rewarded_iOS";

        public string _androidGameId;
        public string _iOsGameId;
        public bool _testMode = true;
        public bool _enablePerPlacementMode = true;
    }
}