using System;
using UnityEngine;
using Zenject;

public class BallLauncherInstaller : MonoInstaller
{
    [SerializeField] LineRenderer lineRenderer;

    [Inject] Settings settings = null;

    public override void InstallBindings()
    {
        Container.Bind<Shooter>().AsCached();

        Container.Bind<Aiming>().To<ScreenWidthAimimgRotator>().AsSingle();
        Container.Bind<TM>().To<TrajectoryManager>().AsSingle();
        Container.Bind<TrajectoryLineRenderer>().AsSingle().WithArguments(lineRenderer);

        Container.Bind<StateWait>().AsSingle();
        Container.Bind<StateAiming>().AsSingle();

        Container.BindInterfacesAndSelfTo<BallLauncherStateManager>().AsSingle();
    }

    [Serializable]
    public class Settings
    {
        public float rotationSpeed = 1f;
        public float anglelimit = 30f;
        public GameObject lastMarker;
        public GameObject dummyBall;
    }
}