﻿using UnityEngine;
using Zenject;

public class AnimationSeter : IInitializable
{
    AnimationClip replasableAnimation, mainAnimation;
    Animator animator;

    AnimatorOverrideController animatorOverride;

    public AnimationSeter(AnimationClip replasableAnimation, AnimationClip mainAnimation, Animator animator)
    {
        this.replasableAnimation = replasableAnimation;
        this.mainAnimation = mainAnimation;
        this.animator = animator;

        animatorOverride = new AnimatorOverrideController(animator.runtimeAnimatorController);
    }

    public void Initialize()
    {
        if (mainAnimation == null) return;

        animator.runtimeAnimatorController = animatorOverride;

        animatorOverride[replasableAnimation.name] = mainAnimation;
    }
}