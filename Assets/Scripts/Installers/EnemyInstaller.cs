using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemyInstaller : MonoInstaller
{
    [Inject]
    Settings settings = null;

    [SerializeField] protected AnimationClip replasableAnimation, mainAnimation;
    [SerializeField] protected Collider mainCollider;

    public override void InstallBindings()
    {
        Container.Bind<Animator>().FromMethod(GetAnimator);
        Container.Bind<List<Collider>>().FromMethod(GetRagDollColliders);
        Container.Bind<List<Rigidbody>>().FromMethod(GetRagDollRiggirs);

        Container.BindInterfacesAndSelfTo<RagDollController>().AsSingle().WithArguments(mainCollider);
        Container.BindInterfacesAndSelfTo<EnemyStateManager2>().AsSingle();

        Container.BindInterfacesAndSelfTo<AnimationSeter>().AsSingle().WithArguments(replasableAnimation, mainAnimation);
    }

    [System.Serializable]
    public class Settings
    {
        public float reflectionHitForce = 100f;
        public float reflectionBallForce = 2f;
    }

    private Animator GetAnimator() => GetComponentInChildren<Animator>();
    private List<Collider> GetRagDollColliders() => new List<Collider>(transform.GetChild(0).GetComponentsInChildren<Collider>());
    private List<Rigidbody> GetRagDollRiggirs() => new List<Rigidbody>(transform.GetChild(0).GetComponentsInChildren<Rigidbody>());

   
}
