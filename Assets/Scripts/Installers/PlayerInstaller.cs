using UnityEngine;
using Zenject;

public class PlayerInstaller : MonoInstaller
{
    public Animator animator;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<PlayerStateManager>().AsSingle().WithArguments(animator);
    }
}