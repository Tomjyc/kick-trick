using UnityEngine;
using Zenject;

public class LevelInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<LevelStats>().AsSingle();

        Container.Bind<StateWaitWinOrLose>().AsSingle();
        Container.Bind<StateWinLevel>().AsSingle();
        Container.Bind<StateToNextArea>().AsSingle();
        Container.Bind<StatePlayArea>().AsSingle();
        Container.Bind<StateRestartPlayArea>().AsSingle();
        Container.Bind<StateWinPlayArea>().AsSingle();
        Container.Bind<SpateAreaLose>().AsSingle();
        Container.Bind<StateRestarLevel>().AsSingle();
        Container.Bind<StateFirstArea>().AsSingle();

        Container.BindInterfacesAndSelfTo<LevelStateManager>().AsSingle();
    }
}