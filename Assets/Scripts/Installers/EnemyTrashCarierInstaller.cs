using UnityEngine;
using Zenject;

public class EnemyTrashCarierInstaller : EnemyRunInstaller
{
    [SerializeField] CarriedCup cupFacade;
    [SerializeField] float pushCupForce = 3f;

    public override void InstallBindings()
    {
        base.InstallBindings();
    }

    public CarriedCup GetCup() { return cupFacade; }
    public float GetPushForce() { return pushCupForce; }
}