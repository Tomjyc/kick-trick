using UnityEngine;
using Zenject;

//[CreateAssetMenu(fileName = "ScriptableObjectInstaller", menuName = "Installers/ScriptableObjectInstaller")]
public class ScriptableObjectInstaller : ScriptableObjectInstaller<ScriptableObjectInstaller>
{
    public GameInstaller.Settings gameSettings;
    public BallLauncherInstaller.Settings ballLaunchrSettings;
    public EnemyInstaller.Settings enemySettings;

    public override void InstallBindings()
    {
        Container.BindInstance(gameSettings).IfNotBound();
        Container.BindInstance(ballLaunchrSettings).IfNotBound();
        Container.BindInstance(enemySettings).IfNotBound();
    }
}