using PathCreation;
using UnityEngine;

public class EnemyRunInstaller : EnemyInstaller
{
    [SerializeField] PathCreator path;
    [SerializeField] float speed;
    [SerializeField] EndOfPathInstruction endOfPathInstruction = EndOfPathInstruction.Loop;

    public override void InstallBindings()
    {
        base.InstallBindings();

        SetRunerType();
    }

    private void SetRunerType()
    {
        if (endOfPathInstruction.Equals(EndOfPathInstruction.Loop))
            Container.Bind<IRuner>().To<FrontRuner>().AsSingle().WithArguments(transform, path, speed, endOfPathInstruction);
        else
            Container.Bind<IRuner>().To<SideWoker>().AsSingle().WithArguments(transform, path, speed, endOfPathInstruction);
    }
}