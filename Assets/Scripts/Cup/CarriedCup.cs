﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarriedCup : CupFacade
{
    [SerializeField] Transform carriedPoint;

    Rigidbody rb;

    protected override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
    }

    public void ReSet()
    {
        rb.useGravity = false;
        rb.isKinematic = true;

        transform.SetParent(carriedPoint);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public void Push(float force)
    {
        transform.SetParent(null);

        rb.useGravity = true;
        rb.isKinematic = false;

        rb.AddForce(transform.forward * force, ForceMode.Impulse);
    }
}
