﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupSenssor : MonoBehaviour
{
    CupFacade cup;

    void Start()
    {
        cup = GetComponentInParent<CupFacade>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Ball ball = other.gameObject.GetComponent<BallFacade>();
        if (ball != null)
        {
            cup.BallInBin(ball);
        }
    }
}
