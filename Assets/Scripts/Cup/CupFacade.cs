﻿using UnityEngine;

public class CupFacade : MonoBehaviour, IReseteble
{
    int hashWin, hashReSet;
    Animator animator;
    [SerializeField] ParticleSystem particleWin;

    public System.Action HitTheCup;

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();

        hashWin = Animator.StringToHash("Win");
        hashReSet = Animator.StringToHash("ReSet");
    }

    public void BallInBin(Ball ball)
    {
        HitTheCup?.Invoke();
        animator.SetTrigger(hashWin);
        particleWin.Play();
    }

    public void ReSet()
    {
        animator.SetTrigger(hashReSet);
    }
}
